-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 18, 2018 at 11:25 AM
-- Server version: 5.7.21-0ubuntu0.16.04.1
-- PHP Version: 7.0.25-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `packer_accramall`
--
CREATE DATABASE IF NOT EXISTS `packer_accramall` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `packer_accramall`;

-- --------------------------------------------------------

--
-- Table structure for table `access_login_failed`
--

CREATE TABLE `access_login_failed` (
  `id` int(20) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) DEFAULT NULL,
  `ipaddress` varchar(20) NOT NULL DEFAULT '0.0.0.0',
  `hostname` varchar(255) NOT NULL,
  `city_region` text,
  `country` varchar(255) DEFAULT NULL,
  `access_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `access_login_success`
--

CREATE TABLE `access_login_success` (
  `login_id` int(20) NOT NULL,
  `employee_id` varchar(20) NOT NULL,
  `time_in` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `time_out` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `online` int(1) NOT NULL DEFAULT '1',
  `ipaddress` varchar(20) NOT NULL DEFAULT '0.0.0.0',
  `hostname` varchar(255) NOT NULL,
  `city_region` text,
  `country` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `access_roles_priviledges_group`
--

CREATE TABLE `access_roles_priviledges_group` (
  `group_id` int(11) NOT NULL,
  `group_name` varchar(255) NOT NULL,
  `roles` text NOT NULL,
  `priviledges` text NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `access_roles_priviledges_user`
--

CREATE TABLE `access_roles_priviledges_user` (
  `id` int(11) NOT NULL,
  `employee_id` varchar(50) NOT NULL,
  `roles` text NOT NULL,
  `priviledges` text NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `access_roles_priviledges_user`
--

INSERT INTO `access_roles_priviledges_user` (`id`, `employee_id`, `roles`, `priviledges`, `group_id`) VALUES
(1, '1', 'STATISTICS|POS|SUPCUST|PROD|CASH|USERS|ROLES|REPORT|ORDER|SETTINGS|INV|GNL|', '', 0);

-- --------------------------------------------------------

--
-- Stand-in structure for view `access_user_details`
--
CREATE TABLE `access_user_details` (
`id` int(11)
,`Username` varchar(20)
,`Password` varchar(100)
,`Account_Status` tinyint(1)
,`Employee_id` varchar(20)
,`User_Roles` text
,`User_Priviledges` text
,`Group_ID` int(11)
,`Group_Name` varchar(255)
,`Group_Roles` text
,`Group_Priviledges` text
);

-- --------------------------------------------------------

--
-- Table structure for table `access_users`
--

CREATE TABLE `access_users` (
  `id` int(11) NOT NULL,
  `employee_id` varchar(20) NOT NULL,
  `username` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_cs NOT NULL,
  `passwd` varchar(100) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `login_attempt` tinyint(1) NOT NULL DEFAULT '5'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `access_users`
--

INSERT INTO `access_users` (`id`, `employee_id`, `username`, `passwd`, `active`, `login_attempt`) VALUES
(1, 'KAD/SYS/OO1', 'sysadmin', '$2y$10$Dq42O1wYiikvM4nNd5JCuOYzxsRfzXdOakOa0V0KRMcDUNWBUyrJy', 1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `account_types`
--

CREATE TABLE `account_types` (
  `id` int(11) NOT NULL,
  `acc_name` varchar(50) NOT NULL,
  `acc_ref_no` varchar(20) NOT NULL,
  `acc_tax` varchar(5) DEFAULT NULL,
  `acc_desc` varchar(255) NOT NULL,
  `created_by` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `account_types`
--

INSERT INTO `account_types` (`id`, `acc_name`, `acc_ref_no`, `acc_tax`, `acc_desc`, `created_by`) VALUES
(1, 'EXPENCES', 'REF#001', 'Yes', 'EXPENCES', 'KAD/SYS/OO1');

-- --------------------------------------------------------

--
-- Table structure for table `accounts_transactions`
--

CREATE TABLE `accounts_transactions` (
  `auto_id` int(11) NOT NULL,
  `acc_type_id` int(11) NOT NULL,
  `debit_amt` int(11) DEFAULT '0',
  `credit_amt` int(11) DEFAULT '0',
  `balance` int(11) NOT NULL,
  `purpose` text NOT NULL,
  `customer_id` varchar(20) NOT NULL,
  `employee_id` varchar(20) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dashboard_tabs`
--

CREATE TABLE `dashboard_tabs` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `comment` varchar(100) NOT NULL,
  `icon` varchar(50) NOT NULL,
  `link` varchar(50) NOT NULL,
  `bg` varchar(50) NOT NULL,
  `priviledges` varchar(255) NOT NULL,
  `type` varchar(100) NOT NULL,
  `active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `full_product_details`
--
CREATE TABLE `full_product_details` (
`Item_Name` varchar(50)
,`ProductCode` varchar(20)
,`Description` varchar(50)
,`Unit_Qty` int(5)
,`Unit_Price` float
,`Promo_Qty` int(5)
,`Promo_Price` float
,`Current_Qty` int(10)
,`Expiry` varchar(20)
,`Product_ID` int(11)
,`DescriptionID` int(11)
);

-- --------------------------------------------------------

--
-- Table structure for table `general_last_ids`
--

CREATE TABLE `general_last_ids` (
  `id` int(5) NOT NULL,
  `employee_id` tinyint(5) DEFAULT NULL,
  `sup_id` tinyint(2) DEFAULT NULL,
  `prod_id` tinyint(5) DEFAULT NULL,
  `cat_id` tinyint(5) DEFAULT NULL,
  `desc_id` tinyint(5) DEFAULT NULL,
  `invoice_no` tinyint(5) DEFAULT NULL,
  `pur_order_no` tinyint(5) DEFAULT NULL,
  `acct_type_no` tinyint(5) DEFAULT NULL,
  `refcode` varchar(10) DEFAULT NULL,
  `product_code` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `group_roles_priviledges`
--

CREATE TABLE `group_roles_priviledges` (
  `group_id` varchar(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `roles` text NOT NULL,
  `priviledges` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hr_companyinfo`
--

CREATE TABLE `hr_companyinfo` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `tel_1` varchar(20) NOT NULL,
  `tel_2` varchar(20) DEFAULT NULL,
  `fax` varchar(20) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `website` varchar(100) DEFAULT NULL,
  `address` varchar(255) NOT NULL,
  `location` varchar(50) NOT NULL,
  `logo_path` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hr_companyinfo`
--

INSERT INTO `hr_companyinfo` (`id`, `name`, `tel_1`, `tel_2`, `fax`, `email`, `website`, `address`, `location`, `logo_path`) VALUES
(2, 'FOTOSTORE', '0546374468', '0554037645', '', '', '', 'P O Box Office Limited', 'Achimota - Mall', 'resources/images/fotostore.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `hr_countries`
--

CREATE TABLE `hr_countries` (
  `cou_code` char(2) NOT NULL DEFAULT '',
  `cou_name` varchar(80) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hr_countries`
--

INSERT INTO `hr_countries` (`cou_code`, `cou_name`) VALUES
('AD', 'Andorra'),
('AE', 'United Arab Emirates'),
('AF', 'Afghanistan'),
('AG', 'Antigua and Barbuda'),
('AI', 'Anguilla'),
('AL', 'Albania'),
('AM', 'Armenia'),
('AN', 'Netherlands Antilles'),
('AO', 'Angola'),
('AQ', 'Antarctica'),
('AR', 'Argentina'),
('AS', 'American Samoa'),
('AT', 'Austria'),
('AU', 'Australia'),
('AW', 'Aruba'),
('AZ', 'Azerbaijan'),
('BA', 'Bosnia and Herzegovina'),
('BB', 'Barbados'),
('BD', 'Bangladesh'),
('BE', 'Belgium'),
('BF', 'Burkina Faso'),
('BG', 'Bulgaria'),
('BH', 'Bahrain'),
('BI', 'Burundi'),
('BJ', 'Benin'),
('BM', 'Bermuda'),
('BN', 'Brunei Darussalam'),
('BO', 'Bolivia'),
('BR', 'Brazil'),
('BS', 'Bahamas'),
('BT', 'Bhutan'),
('BV', 'Bouvet Island'),
('BW', 'Botswana'),
('BY', 'Belarus'),
('BZ', 'Belize'),
('CA', 'Canada'),
('CC', 'Cocos (Keeling) Islands'),
('CD', 'Congo, the Democratic Republic of the'),
('CF', 'Central African Republic'),
('CG', 'Congo'),
('CH', 'Switzerland'),
('CI', 'Cote D\'Ivoire'),
('CK', 'Cook Islands'),
('CL', 'Chile'),
('CM', 'Cameroon'),
('CN', 'China'),
('CO', 'Colombia'),
('CR', 'Costa Rica'),
('CS', 'Serbia and Montenegro'),
('CU', 'Cuba'),
('CV', 'Cape Verde'),
('CX', 'Christmas Island'),
('CY', 'Cyprus'),
('CZ', 'Czech Republic'),
('DE', 'Germany'),
('DJ', 'Djibouti'),
('DK', 'Denmark'),
('DM', 'Dominica'),
('DO', 'Dominican Republic'),
('DZ', 'Algeria'),
('EC', 'Ecuador'),
('EE', 'Estonia'),
('EG', 'Egypt'),
('EH', 'Western Sahara'),
('ER', 'Eritrea'),
('ES', 'Spain'),
('ET', 'Ethiopia'),
('FI', 'Finland'),
('FJ', 'Fiji'),
('FK', 'Falkland Islands (Malvinas)'),
('FM', 'Micronesia, Federated States of'),
('FO', 'Faroe Islands'),
('FR', 'France'),
('GA', 'Gabon'),
('GB', 'United Kingdom'),
('GD', 'Grenada'),
('GE', 'Georgia'),
('GF', 'French Guiana'),
('GH', 'Ghana'),
('GI', 'Gibraltar'),
('GL', 'Greenland'),
('GM', 'Gambia'),
('GN', 'Guinea'),
('GP', 'Guadeloupe'),
('GQ', 'Equatorial Guinea'),
('GR', 'Greece'),
('GS', 'South Georgia and the South Sandwich Islands'),
('GT', 'Guatemala'),
('GU', 'Guam'),
('GW', 'Guinea-Bissau'),
('GY', 'Guyana'),
('HK', 'Hong Kong'),
('HM', 'Heard Island and Mcdonald Islands'),
('HN', 'Honduras'),
('HR', 'Croatia'),
('HT', 'Haiti'),
('HU', 'Hungary'),
('ID', 'Indonesia'),
('IE', 'Ireland'),
('IL', 'Israel'),
('IN', 'India'),
('IO', 'British Indian Ocean Territory'),
('IQ', 'Iraq'),
('IR', 'Iran, Islamic Republic of'),
('IS', 'Iceland'),
('IT', 'Italy'),
('JM', 'Jamaica'),
('JO', 'Jordan'),
('JP', 'Japan'),
('KE', 'Kenya'),
('KG', 'Kyrgyzstan'),
('KH', 'Cambodia'),
('KI', 'Kiribati'),
('KM', 'Comoros'),
('KN', 'Saint Kitts and Nevis'),
('KP', 'Korea, Democratic People\'s Republic of'),
('KR', 'Korea, Republic of'),
('KW', 'Kuwait'),
('KY', 'Cayman Islands'),
('KZ', 'Kazakhstan'),
('LA', 'Lao People\'s Democratic Republic'),
('LB', 'Lebanon'),
('LC', 'Saint Lucia'),
('LI', 'Liechtenstein'),
('LK', 'Sri Lanka'),
('LR', 'Liberia'),
('LS', 'Lesotho'),
('LT', 'Lithuania'),
('LU', 'Luxembourg'),
('LV', 'Latvia'),
('LY', 'Libyan Arab Jamahiriya'),
('MA', 'Morocco'),
('MC', 'Monaco'),
('MD', 'Moldova, Republic of'),
('MG', 'Madagascar'),
('MH', 'Marshall Islands'),
('MK', 'Macedonia, the Former Yugoslav Republic of'),
('ML', 'Mali'),
('MM', 'Myanmar'),
('MN', 'Mongolia'),
('MO', 'Macao'),
('MP', 'Northern Mariana Islands'),
('MQ', 'Martinique'),
('MR', 'Mauritania'),
('MS', 'Montserrat'),
('MT', 'Malta'),
('MU', 'Mauritius'),
('MV', 'Maldives'),
('MW', 'Malawi'),
('MX', 'Mexico'),
('MY', 'Malaysia'),
('MZ', 'Mozambique'),
('NA', 'Namibia'),
('NC', 'New Caledonia'),
('NE', 'Niger'),
('NF', 'Norfolk Island'),
('NG', 'Nigeria'),
('NI', 'Nicaragua'),
('NL', 'Netherlands'),
('NO', 'Norway'),
('NP', 'Nepal'),
('NR', 'Nauru'),
('NU', 'Niue'),
('NZ', 'New Zealand'),
('OM', 'Oman'),
('PA', 'Panama'),
('PE', 'Peru'),
('PF', 'French Polynesia'),
('PG', 'Papua New Guinea'),
('PH', 'Philippines'),
('PK', 'Pakistan'),
('PL', 'Poland'),
('PM', 'Saint Pierre and Miquelon'),
('PN', 'Pitcairn'),
('PR', 'Puerto Rico'),
('PS', 'Palestinian Territory, Occupied'),
('PT', 'Portugal'),
('PW', 'Palau'),
('PY', 'Paraguay'),
('QA', 'Qatar'),
('RE', 'Reunion'),
('RO', 'Romania'),
('RU', 'Russian Federation'),
('RW', 'Rwanda'),
('SA', 'Saudi Arabia'),
('SB', 'Solomon Islands'),
('SC', 'Seychelles'),
('SD', 'Sudan'),
('SE', 'Sweden'),
('SG', 'Singapore'),
('SH', 'Saint Helena'),
('SI', 'Slovenia'),
('SJ', 'Svalbard and Jan Mayen'),
('SK', 'Slovakia'),
('SL', 'Sierra Leone'),
('SM', 'San Marino'),
('SN', 'Senegal'),
('SO', 'Somalia'),
('SR', 'Suriname'),
('ST', 'Sao Tome and Principe'),
('SV', 'El Salvador'),
('SY', 'Syrian Arab Republic'),
('SZ', 'Swaziland'),
('TC', 'Turks and Caicos Islands'),
('TD', 'Chad'),
('TF', 'French Southern Territories'),
('TG', 'Togo'),
('TH', 'Thailand'),
('TJ', 'Tajikistan'),
('TK', 'Tokelau'),
('TL', 'Timor-Leste'),
('TM', 'Turkmenistan'),
('TN', 'Tunisia'),
('TO', 'Tonga'),
('TR', 'Turkey'),
('TT', 'Trinidad and Tobago'),
('TV', 'Tuvalu'),
('TW', 'Taiwan'),
('TZ', 'Tanzania, United Republic of'),
('UA', 'Ukraine'),
('UG', 'Uganda'),
('UM', 'United States Minor Outlying Islands'),
('US', 'United States'),
('UY', 'Uruguay'),
('UZ', 'Uzbekistan'),
('VA', 'Holy See (Vatican City State)'),
('VC', 'Saint Vincent and the Grenadines'),
('VE', 'Venezuela'),
('VG', 'Virgin Islands, British'),
('VI', 'Virgin Islands, U.s.'),
('VN', 'Viet Nam'),
('VU', 'Vanuatu'),
('WF', 'Wallis and Futuna'),
('WS', 'Samoa'),
('YE', 'Yemen'),
('YT', 'Mayotte'),
('ZA', 'South Africa'),
('ZM', 'Zambia'),
('ZW', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Table structure for table `hr_emp_pers_info`
--

CREATE TABLE `hr_emp_pers_info` (
  `id` int(11) NOT NULL,
  `employee_id` varchar(20) DEFAULT NULL,
  `fullname` varchar(100) NOT NULL,
  `dob` varchar(20) NOT NULL,
  `gender` char(1) NOT NULL,
  `residence` varchar(100) NOT NULL,
  `tel1` varchar(20) NOT NULL,
  `tel2` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `eme_name` varchar(50) NOT NULL,
  `eme_tel` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `hr_emp_pers_info`
--

INSERT INTO `hr_emp_pers_info` (`id`, `employee_id`, `fullname`, `dob`, `gender`, `residence`, `tel1`, `tel2`, `email`, `eme_name`, `eme_tel`) VALUES
(1, 'KAD/SYS/OO1', 'System Developer', '28-03-2017', 'N', 'Amasaman', '0541786220', '0245626487', 'marksbon@gmail.com', 'System Developer', '0541786220');

-- --------------------------------------------------------

--
-- Table structure for table `product_audit`
--

CREATE TABLE `product_audit` (
  `invoice_no` varchar(50) NOT NULL,
  `total_cost` int(11) NOT NULL,
  `sup_id` varchar(20) NOT NULL,
  `vendor` varchar(50) NOT NULL,
  `date_recv` varchar(50) NOT NULL,
  `comb_prod_id` text NOT NULL,
  `comb_unit_price` text NOT NULL,
  `comb_qty` text NOT NULL,
  `employee_id` varchar(20) NOT NULL,
  `comb_cost_p` varchar(255) NOT NULL,
  `comb_desc_id` varchar(255) NOT NULL,
  `comb_unit` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_category`
--

CREATE TABLE `product_category` (
  `cat_id` int(11) NOT NULL,
  `cat_name` varchar(50) NOT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_codes`
--

CREATE TABLE `product_codes` (
  `prod_id` int(11) NOT NULL,
  `prod_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_codes`
--

INSERT INTO `product_codes` (`prod_id`, `prod_name`) VALUES
(1, 'passport photo'),
(2, '2x2 usphoto'),
(3, 'Pc photo'),
(4, '4x6 photo'),
(5, '5x7 photo'),
(6, '5x5 photo'),
(7, 'Wallet photo'),
(8, '6x8 photo'),
(9, '5x10 photo'),
(10, '5x12 photo'),
(11, '5x 8.5'),
(12, '5x14'),
(13, '8x10 photo'),
(14, '8x8 photo'),
(15, '8x12 photo'),
(16, '8x14 photo'),
(17, '10x 12 photo'),
(18, '12x16'),
(19, '16x20 photo'),
(20, '20x24 photo'),
(21, '20x30 photo'),
(22, '24x30 photo'),
(23, '24x36 photo'),
(24, 'Print 10 or more'),
(25, '4x6 photo Print 10 or more'),
(26, '5x7 photo Print 10 or more'),
(27, '5x5 photo Print 10 or more'),
(28, '5x 8.5 photo'),
(29, '5x14 photo'),
(30, '12x16 photo'),
(31, '4x6 inch'),
(32, '5x7 inch'),
(33, '6x8 inch'),
(34, '8x10 inch'),
(35, '8x8 inch'),
(36, '5x 10 inch'),
(37, '5x12 inch'),
(38, '5x14 inch'),
(39, '5x 8.5 inch'),
(40, '8x12 inch'),
(41, '8x14 inch'),
(42, '10x 12 inch'),
(43, '12x16 inch'),
(44, '16x20 inch'),
(45, '20x24 inch'),
(46, '20x30 inch'),
(47, '24x30 inch'),
(48, '24x36 inch'),
(49, '16x20'),
(50, '20x24'),
(51, '20x30'),
(52, '24x36'),
(53, '10x28 glossy|mart'),
(54, '8x10 glossy|mart'),
(55, '12x16 glossy|mart'),
(56, 'Photoshoot(small)'),
(57, 'Photoshoot(Averatge)'),
(58, 'Photoshoot(semiLarge)'),
(59, 'Photoshoot(large)'),
(60, 'Photoshoot(medium)'),
(61, 'Photoshoot(semiMedium)'),
(62, 'Product shoot(small)'),
(63, 'Product shoot(Average)'),
(64, 'ProductShoot(medium)'),
(65, 'Product shoot(semiLarge)'),
(66, 'ProductShoot(large)'),
(67, 'Product shoot(semiMedium)'),
(68, 'UNPRINTED'),
(69, 'PRINTED');

-- --------------------------------------------------------

--
-- Table structure for table `product_customers`
--

CREATE TABLE `product_customers` (
  `id` int(11) NOT NULL,
  `fullname` varchar(50) NOT NULL,
  `mobile_number_1` varchar(20) NOT NULL,
  `mobile_number_2` varchar(20) NOT NULL,
  `address` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `location` varchar(100) NOT NULL,
  `date_created` datetime DEFAULT CURRENT_TIMESTAMP,
  `status` enum('active','inactive','deleted','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_desc`
--

CREATE TABLE `product_desc` (
  `desc_id` int(11) NOT NULL,
  `desc` varchar(50) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `status` enum('active','inactive','deleted','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_desc`
--

INSERT INTO `product_desc` (`desc_id`, `desc`, `cat_id`, `status`) VALUES
(18, 'Studio', 0, 'deleted'),
(19, 'Studio', 0, 'active'),
(20, 'Printing', 0, 'active'),
(21, 'Canvas OLD', 0, 'active'),
(22, 'Frame', 0, 'active'),
(23, 'Texturizing', 0, 'active'),
(24, 'Canvas NEW', 0, 'active'),
(25, 'Photobook', 0, 'active'),
(26, 'SOFTCOPY', 0, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `product_details`
--

CREATE TABLE `product_details` (
  `prod_id` varchar(20) NOT NULL,
  `product_id` varchar(20) DEFAULT NULL,
  `desc_id` int(20) NOT NULL,
  `unit_qty` int(5) NOT NULL,
  `cost_price` float DEFAULT NULL,
  `unit_price` float NOT NULL,
  `avail_qty` int(10) DEFAULT NULL,
  `expiry` varchar(20) DEFAULT NULL,
  `promo_qty` int(5) DEFAULT NULL,
  `promo_price` float DEFAULT NULL,
  `img_path` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_details`
--

INSERT INTO `product_details` (`prod_id`, `product_id`, `desc_id`, `unit_qty`, `cost_price`, `unit_price`, `avail_qty`, `expiry`, `promo_qty`, `promo_price`, `img_path`) VALUES
('39', 'PROD#0001', 19, 1, 15, 15, 992, NULL, 0, 0, 'uploads/product/default_logo.png'),
('40', 'PROD#0002', 20, 1, 15, 15, 996, NULL, 0, 0, 'uploads/product/default_logo.png'),
('41', 'PROD#0003', 19, 1, 5, 5, 972, NULL, 0, 0, 'uploads/product/default_logo.png'),
('42', 'PROD#0004', 19, 1, 5, 5, 996, NULL, 0, 0, 'uploads/product/default_logo.png'),
('43', 'PROD#0005', 19, 1, 5, 5, 9997, NULL, 10, 3, 'uploads/product/default_logo.png'),
('44', 'PROD#0006', 19, 1, 8, 8, 951, NULL, 10, 2, 'uploads/product/default_logo.png'),
('1', 'PROD#0007', 19, 1, 20, 20, 956, NULL, 0, 0, 'uploads/product/default_logo.png'),
('2', 'PROD#0008', 19, 1, 20, 20, 998, NULL, 0, 0, 'uploads/product/default_logo.png'),
('3', 'PROD#0009', 19, 1, 4, 4, 996, NULL, 0, 0, 'uploads/product/default_logo.png'),
('4', 'PROD#00010', 19, 5, 5, 5, 989, NULL, 0, 0, 'uploads/product/default_logo.png'),
('5', 'PROD#0011', 19, 1, 5, 5, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('6', 'PROD#0012', 19, 1, 5, 5, 998, NULL, 0, 0, 'uploads/product/default_logo.png'),
('7', 'PROD#0013', 19, 1, 8, 8, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('8', 'PROD#0014', 19, 1, 8, 8, 999, NULL, 0, 0, 'uploads/product/default_logo.png'),
('9', 'PROD#0015', 19, 1, 8, 8, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('10', 'PROD#0016', 19, 1, 8, 8, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('11', 'PROD#0017', 19, 1, 8, 8, 2000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('12', 'PROD#0018', 19, 1, 10, 10, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('13', 'PROD#0019', 19, 1, 10, 10, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('14', 'PROD#0020', 19, 1, 10, 10, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('15', 'PROD#0021', 19, 1, 12, 12, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('16', 'PROD#0022', 19, 1, 15, 15, 991, NULL, 0, 0, 'uploads/product/default_logo.png'),
('17', 'PROD#0023', 19, 1, 30, 30, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('18', 'PROD#0024', 19, 1, 50, 50, 999, NULL, 0, 0, 'uploads/product/default_logo.png'),
('19', 'PROD#0025', 19, 1, 60, 60, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('20', 'PROD#0026', 19, 1, 80, 80, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('21', 'PROD#0027', 19, 1, 90, 90, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('22', 'PROD#0028', 19, 1, 100, 100, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('23', 'PROD#0029', 19, 1, 150, 150, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('1', 'PROD#0030', 20, 1, 10, 10, 956, NULL, 0, 0, NULL),
('2', 'PROD#0031', 20, 1, 15, 15, 998, NULL, 0, 0, NULL),
('24', 'PROD#0032', 20, 1, 4, 4, 1000, NULL, 10, 2, 'uploads/product/default_logo.png'),
('25', 'PROD#0033', 20, 1, 5, 5, 1000, NULL, 10, 5, 'uploads/product/default_logo.png'),
('26', 'PROD#0034', 20, 1, 5, 5, 1000, NULL, 10, 2, 'uploads/product/default_logo.png'),
('27', 'PROD#0035', 20, 1, 5, 5, 1000, NULL, 10, 2, 'uploads/product/default_logo.png'),
('7', 'PROD#0036', 20, 1, 8, 8, 1000, NULL, 0, 0, NULL),
('8', 'PROD#0037', 20, 1, 8, 8, 999, NULL, 0, 0, NULL),
('9', 'PROD#0038', 20, 1, 8, 8, 1000, NULL, 0, 0, NULL),
('10', 'PROD#0039', 20, 1, 8, 8, 1000, NULL, 0, 0, NULL),
('28', 'PROD#0040', 20, 1, 8, 8, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('29', 'PROD#0041', 20, 1, 10, 10, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('13', 'PROD#0042', 20, 1, 10, 10, 1000, NULL, 0, 0, NULL),
('14', 'PROD#0043', 20, 1, 10, 10, 1000, NULL, 0, 0, NULL),
('15', 'PROD#0044', 20, 1, 12, 12, 1000, NULL, 0, 0, NULL),
('16', 'PROD#0045', 20, 1, 15, 15, 991, NULL, 0, 0, NULL),
('17', 'PROD#0046', 20, 1, 30, 30, 1000, NULL, 0, 0, NULL),
('30', 'PROD#0047', 20, 1, 50, 50, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('19', 'PROD#0048', 20, 1, 60, 60, 1000, NULL, 0, 0, NULL),
('20', 'PROD#0049', 20, 1, 80, 80, 1000, NULL, 0, 0, NULL),
('21', 'PROD#0050', 20, 1, 90, 90, 1000, NULL, 0, 0, NULL),
('22', 'PROD#0051', 20, 1, 100, 100, 1000, NULL, 0, 0, NULL),
('23', 'PROD#0052', 20, 1, 150, 150, 1000, NULL, 0, 0, NULL),
('31', 'PROD#0053', 22, 1, 12, 12, 998, NULL, 0, 0, 'uploads/product/default_logo.png'),
('31', 'PROD#0054', 22, 1, 15, 15, 998, NULL, 0, 0, NULL),
('32', 'PROD#0055', 22, 1, 18, 18, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('32', 'PROD#0056', 22, 1, 20, 20, 1000, NULL, 0, 0, NULL),
('33', 'PROD#0057', 22, 1, 20, 20, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('34', 'PROD#0058', 22, 1, 25, 25, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('35', 'PROD#0059', 22, 1, 25, 25, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('36', 'PROD#0060', 22, 1, 20, 20, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('37', 'PROD#0061', 22, 1, 20, 20, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('38', 'PROD#0062', 22, 1, 25, 25, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('39', 'PROD#0063', 22, 1, 20, 20, 992, NULL, 0, 0, 'uploads/product/default_logo.png'),
('40', 'PROD#0064', 22, 1, 30, 30, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('40', 'PROD#0065', 22, 1, 35, 35, 1000, NULL, 0, 0, NULL),
('41', 'PROD#0066', 22, 1, 40, 40, 972, NULL, 0, 0, 'uploads/product/default_logo.png'),
('41', 'PROD#0067', 22, 1, 35, 35, 972, NULL, 0, 0, NULL),
('42', 'PROD#0068', 22, 1, 70, 70, 996, NULL, 0, 0, 'uploads/product/default_logo.png'),
('42', 'PROD#0069', 22, 1, 50, 50, 996, NULL, 0, 0, NULL),
('43', 'PROD#0070', 22, 1, 70, 70, 9997, NULL, 0, 0, 'uploads/product/default_logo.png'),
('43', 'PROD#0071', 22, 1, 100, 100, 9997, NULL, 0, 0, NULL),
('44', 'PROD#0072', 22, 1, 100, 100, 951, NULL, 0, 0, 'uploads/product/default_logo.png'),
('44', 'PROD#0073', 22, 1, 150, 150, 951, NULL, 0, 0, NULL),
('44', 'PROD#0074', 22, 1, 200, 200, 951, NULL, 0, 0, NULL),
('45', 'PROD#0075', 22, 1, 350, 350, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('45', 'PROD#0076', 22, 1, 300, 300, 1000, NULL, 0, 0, NULL),
('45', 'PROD#0077', 22, 1, 250, 250, 1000, NULL, 0, 0, NULL),
('46', 'PROD#0078', 22, 1, 450, 450, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('46', 'PROD#0079', 22, 1, 350, 350, 1000, NULL, 0, 0, NULL),
('47', 'PROD#0080', 22, 1, 500, 500, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('47', 'PROD#0081', 22, 1, 450, 450, 1000, NULL, 0, 0, NULL),
('48', 'PROD#0082', 22, 1, 500, 500, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('48', 'PROD#0083', 22, 1, 600, 600, 1000, NULL, 0, 0, NULL),
('31', 'PROD#0084', 23, 1, 20, 20, 998, NULL, 0, 0, NULL),
('32', 'PROD#0085', 23, 1, 25, 25, 1000, NULL, 0, 0, NULL),
('33', 'PROD#0086', 23, 1, 30, 30, 1000, NULL, 0, 0, NULL),
('34', 'PROD#0087', 23, 1, 40, 40, 1000, NULL, 0, 0, NULL),
('35', 'PROD#0088', 23, 1, 40, 40, 1000, NULL, 0, 0, NULL),
('36', 'PROD#0089', 23, 1, 30, 30, 1000, NULL, 0, 0, NULL),
('10', 'PROD#0090', 23, 1, 30, 30, 1000, NULL, 0, 0, NULL),
('38', 'PROD#0091', 23, 1, 30, 30, 1000, NULL, 0, 0, NULL),
('39', 'PROD#0092', 23, 1, 25, 25, 992, NULL, 0, 0, NULL),
('40', 'PROD#0093', 23, 1, 35, 35, 1000, NULL, 0, 0, NULL),
('41', 'PROD#0094', 23, 1, 40, 40, 972, NULL, 0, 0, NULL),
('42', 'PROD#0095', 23, 1, 60, 60, 996, NULL, 0, 0, NULL),
('43', 'PROD#0096', 23, 1, 80, 80, 9997, NULL, 0, 0, NULL),
('44', 'PROD#0097', 23, 1, 100, 100, 951, NULL, 0, 0, NULL),
('45', 'PROD#0098', 23, 1, 150, 150, 1000, NULL, 0, 0, NULL),
('46', 'PROD#0099', 23, 1, 200, 200, 1000, NULL, 0, 0, NULL),
('47', 'PROD#00100', 23, 1, 250, 250, 1000, NULL, 0, 0, NULL),
('48', 'PROD#101', 23, 1, 300, 300, 1000, NULL, 0, 0, NULL),
('18', 'PROD#102', 21, 1, 200, 200, 999, NULL, 0, 0, NULL),
('49', 'PROD#103', 21, 1, 250, 250, 996, NULL, 0, 0, 'uploads/product/default_logo.png'),
('50', 'PROD#104', 21, 1, 400, 400, 990, NULL, 0, 0, 'uploads/product/default_logo.png'),
('51', 'PROD#105', 21, 1, 450, 450, 992, NULL, 0, 0, 'uploads/product/default_logo.png'),
('52', 'PROD#106', 21, 1, 550, 550, 977, NULL, 0, 0, 'uploads/product/default_logo.png'),
('18', 'PROD#107', 24, 1, 250, 250, 999, NULL, 0, 0, NULL),
('49', 'PROD#108', 24, 1, 300, 300, 996, NULL, 0, 0, NULL),
('50', 'PROD#109', 24, 1, 450, 450, 990, NULL, 0, 0, NULL),
('51', 'PROD#110', 24, 1, 500, 500, 992, NULL, 0, 0, NULL),
('52', 'PROD#111', 24, 1, 600, 600, 977, NULL, 0, 0, NULL),
('53', 'PROD#112', 25, 1, 900, 900, 2000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('54', 'PROD#113', 25, 1, 600, 600, 2000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('55', 'PROD#114', 25, 1, 1200, 1200, 2000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('56', 'PROD#115', 20, 1, 150, 150, 2000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('57', 'PROD#116', 20, 1, 200, 200, 2000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('58', 'PROD#117', 20, 1, 300, 300, 2000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('59', 'PROD#118', 20, 1, 400, 400, 2000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('56', 'PROD#119', 19, 1, 60, 60, 2000, NULL, 0, 0, NULL),
('57', 'PROD#120', 19, 1, 100, 100, 2000, NULL, 0, 0, NULL),
('60', 'PROD#121', 19, 1, 200, 200, 2000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('58', 'PROD#122', 19, 1, 300, 300, 2000, NULL, 0, 0, NULL),
('59', 'PROD#123', 19, 1, 400, 400, 2000, NULL, 0, 0, NULL),
('61', 'PROD#124', 19, 1, 150, 150, 1999, NULL, 0, 0, 'uploads/product/default_logo.png'),
('62', 'PROD#125', 19, 1, 60, 60, 2000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('63', 'PROD#126', 19, 1, 100, 100, 2000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('64', 'PROD#127', 19, 1, 200, 200, 2000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('65', 'PROD#128', 19, 1, 300, 300, 2000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('66', 'PROD#129', 19, 1, 400, 400, 2000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('61', 'PROD#130', 19, 1, 150, 150, 1999, NULL, 0, 0, NULL),
('67', 'PROD#131', 19, 1, 150, 150, 2000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('68', 'PROD#132', 26, 1, 0, 5, 989, NULL, 0, 0, 'uploads/product/default_logo.png'),
('69', 'PROD#133', 26, 1, 0, 2, 999994, NULL, 0, 0, 'uploads/product/default_logo.png');

-- --------------------------------------------------------

--
-- Table structure for table `product_suppliers`
--

CREATE TABLE `product_suppliers` (
  `sup_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `tel1` varchar(20) NOT NULL,
  `tel2` varchar(20) DEFAULT NULL,
  `addr` varchar(255) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `loc` varchar(50) NOT NULL,
  `pay_type` varchar(20) DEFAULT NULL,
  `prod_type` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_suppliers_accounts`
--

CREATE TABLE `product_suppliers_accounts` (
  `id` int(11) NOT NULL,
  `sup_id` int(11) NOT NULL,
  `bank` varchar(50) NOT NULL,
  `branch` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `num` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_transact`
--

CREATE TABLE `product_transact` (
  `id` int(11) NOT NULL,
  `description` varchar(100) NOT NULL,
  `prod_id` varchar(50) NOT NULL,
  `qty_withdrawn` varchar(50) NOT NULL,
  `unit_price` varchar(50) NOT NULL,
  `tot_cost` int(11) NOT NULL,
  `tax` double NOT NULL,
  `sold_by` varchar(50) NOT NULL,
  `customer` varchar(150) NOT NULL,
  `date_withdrawn` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `employee_id` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `report_dailyledger`
--
CREATE TABLE `report_dailyledger` (
`Autoid` int(11)
,`Description` varchar(100)
,`CostPrice` float
,`Qty_Bought` varchar(50)
,`SellingPrice` float
,`T_Cost` double
,`Tax` double
);

-- --------------------------------------------------------

--
-- Table structure for table `report_purchaseorder`
--

CREATE TABLE `report_purchaseorder` (
  `id` int(11) NOT NULL,
  `sup_id` int(11) NOT NULL,
  `invoice` int(5) NOT NULL,
  `tot_cost` float NOT NULL,
  `products` text NOT NULL,
  `quantity` text NOT NULL,
  `prices` text NOT NULL,
  `remarks` varchar(255) NOT NULL,
  `pay` float NOT NULL,
  `bal` float NOT NULL,
  `date_created` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reserved_license_keys`
--

CREATE TABLE `reserved_license_keys` (
  `license_id` int(20) NOT NULL,
  `key` varchar(100) NOT NULL,
  `end_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reserved_registered_keys`
--

CREATE TABLE `reserved_registered_keys` (
  `reg_key_id` int(20) NOT NULL,
  `license_id` int(20) NOT NULL,
  `client_id` int(11) NOT NULL,
  `state` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `suppliers_full_details`
--
CREATE TABLE `suppliers_full_details` (
`sup_id` int(11)
,`name` varchar(255)
,`addr` varchar(255)
,`loc` varchar(50)
,`tel1` varchar(20)
,`tel2` varchar(20)
,`email` varchar(100)
,`prod_type` text
,`pay_type` varchar(20)
,`bank` varchar(50)
,`branch` varchar(50)
,`acctname` varchar(100)
,`num` varchar(25)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_account_details`
--
CREATE TABLE `v_account_details` (
`Id` int(11)
,`Name` varchar(50)
,`Refcon` varchar(20)
,`Descrip` varchar(255)
,`Debit_Amt` int(11)
,`Credit_Amt` int(11)
,`Balance` bigint(12)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_product_transact`
--
CREATE TABLE `vw_product_transact` (
`id` int(11)
,`description` varchar(100)
,`prod_id` varchar(50)
,`qty_withdrawn` varchar(50)
,`unit_price` varchar(50)
,`tot_cost` int(11)
,`tax` double
,`date_withdrawn` datetime
,`employee_id` varchar(20)
,`tax_value` double
);

-- --------------------------------------------------------

--
-- Structure for view `access_user_details`
--
DROP TABLE IF EXISTS `access_user_details`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `access_user_details`  AS  select `access_users`.`id` AS `id`,`access_users`.`username` AS `Username`,`access_users`.`passwd` AS `Password`,`access_users`.`active` AS `Account_Status`,`hr_emp_pers_info`.`employee_id` AS `Employee_id`,`access_roles_priviledges_user`.`roles` AS `User_Roles`,`access_roles_priviledges_user`.`priviledges` AS `User_Priviledges`,`access_roles_priviledges_user`.`group_id` AS `Group_ID`,`access_roles_priviledges_group`.`group_name` AS `Group_Name`,`access_roles_priviledges_group`.`roles` AS `Group_Roles`,`access_roles_priviledges_group`.`priviledges` AS `Group_Priviledges` from (((`access_users` left join `hr_emp_pers_info` on((`hr_emp_pers_info`.`id` = `access_users`.`id`))) left join `access_roles_priviledges_user` on((`access_roles_priviledges_user`.`employee_id` = `hr_emp_pers_info`.`id`))) left join `access_roles_priviledges_group` on((`access_roles_priviledges_group`.`group_id` = `access_roles_priviledges_user`.`group_id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `full_product_details`
--
DROP TABLE IF EXISTS `full_product_details`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `full_product_details`  AS  select `product_codes`.`prod_name` AS `Item_Name`,`product_details`.`product_id` AS `ProductCode`,`product_desc`.`desc` AS `Description`,`product_details`.`unit_qty` AS `Unit_Qty`,`product_details`.`unit_price` AS `Unit_Price`,`product_details`.`promo_qty` AS `Promo_Qty`,`product_details`.`promo_price` AS `Promo_Price`,`product_details`.`avail_qty` AS `Current_Qty`,`product_details`.`expiry` AS `Expiry`,`product_codes`.`prod_id` AS `Product_ID`,`product_desc`.`desc_id` AS `DescriptionID` from ((`product_codes` join `product_details` on((`product_details`.`prod_id` = `product_codes`.`prod_id`))) join `product_desc` on((`product_desc`.`desc_id` = `product_details`.`desc_id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `report_dailyledger`
--
DROP TABLE IF EXISTS `report_dailyledger`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `report_dailyledger`  AS  (select `product_transact`.`id` AS `Autoid`,`product_transact`.`description` AS `Description`,`product_details`.`cost_price` AS `CostPrice`,`product_transact`.`qty_withdrawn` AS `Qty_Bought`,`product_details`.`unit_price` AS `SellingPrice`,(`product_details`.`unit_price` * `product_transact`.`qty_withdrawn`) AS `T_Cost`,((`product_details`.`unit_price` * `product_transact`.`qty_withdrawn`) * 0.175) AS `Tax` from (`product_transact` join `product_details` on((`product_details`.`prod_id` = `product_transact`.`prod_id`)))) ;

-- --------------------------------------------------------

--
-- Structure for view `suppliers_full_details`
--
DROP TABLE IF EXISTS `suppliers_full_details`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `suppliers_full_details`  AS  (select `product_suppliers`.`sup_id` AS `sup_id`,`product_suppliers`.`name` AS `name`,`product_suppliers`.`addr` AS `addr`,`product_suppliers`.`loc` AS `loc`,`product_suppliers`.`tel1` AS `tel1`,`product_suppliers`.`tel2` AS `tel2`,`product_suppliers`.`email` AS `email`,`product_suppliers`.`prod_type` AS `prod_type`,`product_suppliers`.`pay_type` AS `pay_type`,`product_suppliers_accounts`.`bank` AS `bank`,`product_suppliers_accounts`.`branch` AS `branch`,`product_suppliers_accounts`.`name` AS `acctname`,`product_suppliers_accounts`.`num` AS `num` from (`product_suppliers` left join `product_suppliers_accounts` on((`product_suppliers_accounts`.`sup_id` = `product_suppliers`.`sup_id`)))) ;

-- --------------------------------------------------------

--
-- Structure for view `v_account_details`
--
DROP TABLE IF EXISTS `v_account_details`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_account_details`  AS  select `account_types`.`id` AS `Id`,`account_types`.`acc_name` AS `Name`,`account_types`.`acc_ref_no` AS `Refcon`,`account_types`.`acc_desc` AS `Descrip`,`accounts_transactions`.`debit_amt` AS `Debit_Amt`,`accounts_transactions`.`credit_amt` AS `Credit_Amt`,(`accounts_transactions`.`credit_amt` - `accounts_transactions`.`debit_amt`) AS `Balance` from (`account_types` left join `accounts_transactions` on((`accounts_transactions`.`acc_type_id` = `account_types`.`id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `vw_product_transact`
--
DROP TABLE IF EXISTS `vw_product_transact`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_product_transact`  AS  select `a`.`id` AS `id`,`a`.`description` AS `description`,`a`.`prod_id` AS `prod_id`,`a`.`qty_withdrawn` AS `qty_withdrawn`,`a`.`unit_price` AS `unit_price`,`a`.`tot_cost` AS `tot_cost`,`a`.`tax` AS `tax`,`a`.`date_withdrawn` AS `date_withdrawn`,`a`.`employee_id` AS `employee_id`,((`a`.`tax` * `a`.`tot_cost`) / 100) AS `tax_value` from `product_transact` `a` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access_login_failed`
--
ALTER TABLE `access_login_failed`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `access_login_success`
--
ALTER TABLE `access_login_success`
  ADD PRIMARY KEY (`login_id`);

--
-- Indexes for table `access_roles_priviledges_group`
--
ALTER TABLE `access_roles_priviledges_group`
  ADD PRIMARY KEY (`group_id`),
  ADD UNIQUE KEY `name` (`group_name`);

--
-- Indexes for table `access_roles_priviledges_user`
--
ALTER TABLE `access_roles_priviledges_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `employee_id_UNIQUE` (`employee_id`);

--
-- Indexes for table `access_users`
--
ALTER TABLE `access_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username_UNIQUE` (`username`);

--
-- Indexes for table `account_types`
--
ALTER TABLE `account_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `accounts_transactions`
--
ALTER TABLE `accounts_transactions`
  ADD PRIMARY KEY (`auto_id`);

--
-- Indexes for table `dashboard_tabs`
--
ALTER TABLE `dashboard_tabs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_last_ids`
--
ALTER TABLE `general_last_ids`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group_roles_priviledges`
--
ALTER TABLE `group_roles_priviledges`
  ADD PRIMARY KEY (`group_id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `hr_companyinfo`
--
ALTER TABLE `hr_companyinfo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hr_countries`
--
ALTER TABLE `hr_countries`
  ADD PRIMARY KEY (`cou_code`);

--
-- Indexes for table `hr_emp_pers_info`
--
ALTER TABLE `hr_emp_pers_info`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQUE` (`employee_id`,`tel1`);

--
-- Indexes for table `product_category`
--
ALTER TABLE `product_category`
  ADD PRIMARY KEY (`cat_id`),
  ADD UNIQUE KEY `cat_name_UNIQUE` (`cat_name`);

--
-- Indexes for table `product_codes`
--
ALTER TABLE `product_codes`
  ADD PRIMARY KEY (`prod_id`),
  ADD UNIQUE KEY `prod_id_UNIQUE` (`prod_id`);

--
-- Indexes for table `product_customers`
--
ALTER TABLE `product_customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_desc`
--
ALTER TABLE `product_desc`
  ADD PRIMARY KEY (`desc_id`);

--
-- Indexes for table `product_details`
--
ALTER TABLE `product_details`
  ADD UNIQUE KEY `UNIQUE` (`product_id`);

--
-- Indexes for table `product_suppliers`
--
ALTER TABLE `product_suppliers`
  ADD PRIMARY KEY (`sup_id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `product_suppliers_accounts`
--
ALTER TABLE `product_suppliers_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_transact`
--
ALTER TABLE `product_transact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `report_purchaseorder`
--
ALTER TABLE `report_purchaseorder`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reserved_license_keys`
--
ALTER TABLE `reserved_license_keys`
  ADD PRIMARY KEY (`license_id`,`key`,`end_date`),
  ADD UNIQUE KEY `UNIQUE` (`key`);

--
-- Indexes for table `reserved_registered_keys`
--
ALTER TABLE `reserved_registered_keys`
  ADD PRIMARY KEY (`reg_key_id`),
  ADD UNIQUE KEY `UNIQUE` (`license_id`,`client_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `access_login_failed`
--
ALTER TABLE `access_login_failed`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `access_login_success`
--
ALTER TABLE `access_login_success`
  MODIFY `login_id` int(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `access_roles_priviledges_group`
--
ALTER TABLE `access_roles_priviledges_group`
  MODIFY `group_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `access_roles_priviledges_user`
--
ALTER TABLE `access_roles_priviledges_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `access_users`
--
ALTER TABLE `access_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `account_types`
--
ALTER TABLE `account_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `accounts_transactions`
--
ALTER TABLE `accounts_transactions`
  MODIFY `auto_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `dashboard_tabs`
--
ALTER TABLE `dashboard_tabs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `general_last_ids`
--
ALTER TABLE `general_last_ids`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `hr_companyinfo`
--
ALTER TABLE `hr_companyinfo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `hr_emp_pers_info`
--
ALTER TABLE `hr_emp_pers_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `product_category`
--
ALTER TABLE `product_category`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_codes`
--
ALTER TABLE `product_codes`
  MODIFY `prod_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;
--
-- AUTO_INCREMENT for table `product_customers`
--
ALTER TABLE `product_customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_desc`
--
ALTER TABLE `product_desc`
  MODIFY `desc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `product_suppliers`
--
ALTER TABLE `product_suppliers`
  MODIFY `sup_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_suppliers_accounts`
--
ALTER TABLE `product_suppliers_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_transact`
--
ALTER TABLE `product_transact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `report_purchaseorder`
--
ALTER TABLE `report_purchaseorder`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `reserved_license_keys`
--
ALTER TABLE `reserved_license_keys`
  MODIFY `license_id` int(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `reserved_registered_keys`
--
ALTER TABLE `reserved_registered_keys`
  MODIFY `reg_key_id` int(20) NOT NULL AUTO_INCREMENT;--
-- Database: `packer_admin`
--
CREATE DATABASE IF NOT EXISTS `packer_admin` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `packer_admin`;

-- --------------------------------------------------------

--
-- Table structure for table `access_login_failed`
--

CREATE TABLE `access_login_failed` (
  `id` int(20) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) DEFAULT NULL,
  `ipaddress` varchar(20) NOT NULL DEFAULT '0.0.0.0',
  `hostname` varchar(255) NOT NULL,
  `city_region` text,
  `country` varchar(255) DEFAULT NULL,
  `access_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `access_login_success`
--

CREATE TABLE `access_login_success` (
  `login_id` int(20) NOT NULL,
  `employee_id` varchar(20) NOT NULL,
  `time_in` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `time_out` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `online` int(1) NOT NULL DEFAULT '1',
  `ipaddress` varchar(20) NOT NULL DEFAULT '0.0.0.0',
  `hostname` varchar(255) NOT NULL,
  `city_region` text,
  `country` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `access_roles_priviledges_group`
--

CREATE TABLE `access_roles_priviledges_group` (
  `group_id` int(11) NOT NULL,
  `group_name` varchar(255) NOT NULL,
  `roles` text NOT NULL,
  `priviledges` text NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `access_roles_priviledges_user`
--

CREATE TABLE `access_roles_priviledges_user` (
  `id` int(11) NOT NULL,
  `employee_id` varchar(50) NOT NULL,
  `roles` text NOT NULL,
  `priviledges` text NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `access_roles_priviledges_user`
--

INSERT INTO `access_roles_priviledges_user` (`id`, `employee_id`, `roles`, `priviledges`, `group_id`) VALUES
(1, '1', 'STATISTICS|POS|SUPCUST|PROD|CASH|USERS|ROLES|REPORT|ORDER|SETTINGS|INV|GNL|', '', 0);

-- --------------------------------------------------------

--
-- Stand-in structure for view `access_user_details`
--
CREATE TABLE `access_user_details` (
`id` int(11)
,`Username` varchar(20)
,`Password` varchar(100)
,`Account_Status` tinyint(1)
,`Employee_id` varchar(20)
,`User_Roles` text
,`User_Priviledges` text
,`Group_ID` int(11)
,`Group_Name` varchar(255)
,`Group_Roles` text
,`Group_Priviledges` text
);

-- --------------------------------------------------------

--
-- Table structure for table `access_users`
--

CREATE TABLE `access_users` (
  `id` int(11) NOT NULL,
  `employee_id` varchar(20) NOT NULL,
  `username` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_cs NOT NULL,
  `passwd` varchar(100) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `login_attempt` tinyint(1) NOT NULL DEFAULT '5'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `access_users`
--

INSERT INTO `access_users` (`id`, `employee_id`, `username`, `passwd`, `active`, `login_attempt`) VALUES
(1, 'KAD/SYS/OO1', 'sysadmin', '$2y$10$Dq42O1wYiikvM4nNd5JCuOYzxsRfzXdOakOa0V0KRMcDUNWBUyrJy', 1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `account_types`
--

CREATE TABLE `account_types` (
  `id` int(11) NOT NULL,
  `acc_name` varchar(50) NOT NULL,
  `acc_ref_no` varchar(20) NOT NULL,
  `acc_tax` varchar(5) DEFAULT NULL,
  `acc_desc` varchar(255) NOT NULL,
  `created_by` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `account_types`
--

INSERT INTO `account_types` (`id`, `acc_name`, `acc_ref_no`, `acc_tax`, `acc_desc`, `created_by`) VALUES
(1, 'EXPENCES', 'REF#001', 'Yes', 'EXPENCES', 'KAD/SYS/OO1');

-- --------------------------------------------------------

--
-- Table structure for table `accounts_transactions`
--

CREATE TABLE `accounts_transactions` (
  `auto_id` int(11) NOT NULL,
  `acc_type_id` int(11) NOT NULL,
  `debit_amt` int(11) DEFAULT '0',
  `credit_amt` int(11) DEFAULT '0',
  `balance` int(11) NOT NULL,
  `purpose` text NOT NULL,
  `customer_id` varchar(20) NOT NULL,
  `employee_id` varchar(20) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dashboard_tabs`
--

CREATE TABLE `dashboard_tabs` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `comment` varchar(100) NOT NULL,
  `icon` varchar(50) NOT NULL,
  `link` varchar(50) NOT NULL,
  `bg` varchar(50) NOT NULL,
  `priviledges` varchar(255) NOT NULL,
  `type` varchar(100) NOT NULL,
  `active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `full_product_details`
--
CREATE TABLE `full_product_details` (
`Item_Name` varchar(50)
,`ProductCode` varchar(20)
,`Description` varchar(50)
,`Unit_Qty` int(5)
,`Unit_Price` float
,`Promo_Qty` int(5)
,`Promo_Price` float
,`Current_Qty` int(10)
,`Expiry` varchar(20)
,`Product_ID` int(11)
,`DescriptionID` int(11)
);

-- --------------------------------------------------------

--
-- Table structure for table `general_last_ids`
--

CREATE TABLE `general_last_ids` (
  `id` int(5) NOT NULL,
  `employee_id` tinyint(5) DEFAULT NULL,
  `sup_id` tinyint(2) DEFAULT NULL,
  `prod_id` tinyint(5) DEFAULT NULL,
  `cat_id` tinyint(5) DEFAULT NULL,
  `desc_id` tinyint(5) DEFAULT NULL,
  `invoice_no` tinyint(5) DEFAULT NULL,
  `pur_order_no` tinyint(5) DEFAULT NULL,
  `acct_type_no` tinyint(5) DEFAULT NULL,
  `refcode` varchar(10) DEFAULT NULL,
  `product_code` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `group_roles_priviledges`
--

CREATE TABLE `group_roles_priviledges` (
  `group_id` varchar(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `roles` text NOT NULL,
  `priviledges` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hr_companyinfo`
--

CREATE TABLE `hr_companyinfo` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `tel_1` varchar(20) NOT NULL,
  `tel_2` varchar(20) DEFAULT NULL,
  `fax` varchar(20) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `website` varchar(100) DEFAULT NULL,
  `address` varchar(255) NOT NULL,
  `location` varchar(50) NOT NULL,
  `logo_path` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hr_companyinfo`
--

INSERT INTO `hr_companyinfo` (`id`, `name`, `tel_1`, `tel_2`, `fax`, `email`, `website`, `address`, `location`, `logo_path`) VALUES
(2, 'FOTOSTORE', '0546374468', '0554037645', '', '', '', 'P O Box Office Limited', 'Achimota - Mall', 'resources/images/fotostore.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `hr_countries`
--

CREATE TABLE `hr_countries` (
  `cou_code` char(2) NOT NULL DEFAULT '',
  `cou_name` varchar(80) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hr_countries`
--

INSERT INTO `hr_countries` (`cou_code`, `cou_name`) VALUES
('AD', 'Andorra'),
('AE', 'United Arab Emirates'),
('AF', 'Afghanistan'),
('AG', 'Antigua and Barbuda'),
('AI', 'Anguilla'),
('AL', 'Albania'),
('AM', 'Armenia'),
('AN', 'Netherlands Antilles'),
('AO', 'Angola'),
('AQ', 'Antarctica'),
('AR', 'Argentina'),
('AS', 'American Samoa'),
('AT', 'Austria'),
('AU', 'Australia'),
('AW', 'Aruba'),
('AZ', 'Azerbaijan'),
('BA', 'Bosnia and Herzegovina'),
('BB', 'Barbados'),
('BD', 'Bangladesh'),
('BE', 'Belgium'),
('BF', 'Burkina Faso'),
('BG', 'Bulgaria'),
('BH', 'Bahrain'),
('BI', 'Burundi'),
('BJ', 'Benin'),
('BM', 'Bermuda'),
('BN', 'Brunei Darussalam'),
('BO', 'Bolivia'),
('BR', 'Brazil'),
('BS', 'Bahamas'),
('BT', 'Bhutan'),
('BV', 'Bouvet Island'),
('BW', 'Botswana'),
('BY', 'Belarus'),
('BZ', 'Belize'),
('CA', 'Canada'),
('CC', 'Cocos (Keeling) Islands'),
('CD', 'Congo, the Democratic Republic of the'),
('CF', 'Central African Republic'),
('CG', 'Congo'),
('CH', 'Switzerland'),
('CI', 'Cote D\'Ivoire'),
('CK', 'Cook Islands'),
('CL', 'Chile'),
('CM', 'Cameroon'),
('CN', 'China'),
('CO', 'Colombia'),
('CR', 'Costa Rica'),
('CS', 'Serbia and Montenegro'),
('CU', 'Cuba'),
('CV', 'Cape Verde'),
('CX', 'Christmas Island'),
('CY', 'Cyprus'),
('CZ', 'Czech Republic'),
('DE', 'Germany'),
('DJ', 'Djibouti'),
('DK', 'Denmark'),
('DM', 'Dominica'),
('DO', 'Dominican Republic'),
('DZ', 'Algeria'),
('EC', 'Ecuador'),
('EE', 'Estonia'),
('EG', 'Egypt'),
('EH', 'Western Sahara'),
('ER', 'Eritrea'),
('ES', 'Spain'),
('ET', 'Ethiopia'),
('FI', 'Finland'),
('FJ', 'Fiji'),
('FK', 'Falkland Islands (Malvinas)'),
('FM', 'Micronesia, Federated States of'),
('FO', 'Faroe Islands'),
('FR', 'France'),
('GA', 'Gabon'),
('GB', 'United Kingdom'),
('GD', 'Grenada'),
('GE', 'Georgia'),
('GF', 'French Guiana'),
('GH', 'Ghana'),
('GI', 'Gibraltar'),
('GL', 'Greenland'),
('GM', 'Gambia'),
('GN', 'Guinea'),
('GP', 'Guadeloupe'),
('GQ', 'Equatorial Guinea'),
('GR', 'Greece'),
('GS', 'South Georgia and the South Sandwich Islands'),
('GT', 'Guatemala'),
('GU', 'Guam'),
('GW', 'Guinea-Bissau'),
('GY', 'Guyana'),
('HK', 'Hong Kong'),
('HM', 'Heard Island and Mcdonald Islands'),
('HN', 'Honduras'),
('HR', 'Croatia'),
('HT', 'Haiti'),
('HU', 'Hungary'),
('ID', 'Indonesia'),
('IE', 'Ireland'),
('IL', 'Israel'),
('IN', 'India'),
('IO', 'British Indian Ocean Territory'),
('IQ', 'Iraq'),
('IR', 'Iran, Islamic Republic of'),
('IS', 'Iceland'),
('IT', 'Italy'),
('JM', 'Jamaica'),
('JO', 'Jordan'),
('JP', 'Japan'),
('KE', 'Kenya'),
('KG', 'Kyrgyzstan'),
('KH', 'Cambodia'),
('KI', 'Kiribati'),
('KM', 'Comoros'),
('KN', 'Saint Kitts and Nevis'),
('KP', 'Korea, Democratic People\'s Republic of'),
('KR', 'Korea, Republic of'),
('KW', 'Kuwait'),
('KY', 'Cayman Islands'),
('KZ', 'Kazakhstan'),
('LA', 'Lao People\'s Democratic Republic'),
('LB', 'Lebanon'),
('LC', 'Saint Lucia'),
('LI', 'Liechtenstein'),
('LK', 'Sri Lanka'),
('LR', 'Liberia'),
('LS', 'Lesotho'),
('LT', 'Lithuania'),
('LU', 'Luxembourg'),
('LV', 'Latvia'),
('LY', 'Libyan Arab Jamahiriya'),
('MA', 'Morocco'),
('MC', 'Monaco'),
('MD', 'Moldova, Republic of'),
('MG', 'Madagascar'),
('MH', 'Marshall Islands'),
('MK', 'Macedonia, the Former Yugoslav Republic of'),
('ML', 'Mali'),
('MM', 'Myanmar'),
('MN', 'Mongolia'),
('MO', 'Macao'),
('MP', 'Northern Mariana Islands'),
('MQ', 'Martinique'),
('MR', 'Mauritania'),
('MS', 'Montserrat'),
('MT', 'Malta'),
('MU', 'Mauritius'),
('MV', 'Maldives'),
('MW', 'Malawi'),
('MX', 'Mexico'),
('MY', 'Malaysia'),
('MZ', 'Mozambique'),
('NA', 'Namibia'),
('NC', 'New Caledonia'),
('NE', 'Niger'),
('NF', 'Norfolk Island'),
('NG', 'Nigeria'),
('NI', 'Nicaragua'),
('NL', 'Netherlands'),
('NO', 'Norway'),
('NP', 'Nepal'),
('NR', 'Nauru'),
('NU', 'Niue'),
('NZ', 'New Zealand'),
('OM', 'Oman'),
('PA', 'Panama'),
('PE', 'Peru'),
('PF', 'French Polynesia'),
('PG', 'Papua New Guinea'),
('PH', 'Philippines'),
('PK', 'Pakistan'),
('PL', 'Poland'),
('PM', 'Saint Pierre and Miquelon'),
('PN', 'Pitcairn'),
('PR', 'Puerto Rico'),
('PS', 'Palestinian Territory, Occupied'),
('PT', 'Portugal'),
('PW', 'Palau'),
('PY', 'Paraguay'),
('QA', 'Qatar'),
('RE', 'Reunion'),
('RO', 'Romania'),
('RU', 'Russian Federation'),
('RW', 'Rwanda'),
('SA', 'Saudi Arabia'),
('SB', 'Solomon Islands'),
('SC', 'Seychelles'),
('SD', 'Sudan'),
('SE', 'Sweden'),
('SG', 'Singapore'),
('SH', 'Saint Helena'),
('SI', 'Slovenia'),
('SJ', 'Svalbard and Jan Mayen'),
('SK', 'Slovakia'),
('SL', 'Sierra Leone'),
('SM', 'San Marino'),
('SN', 'Senegal'),
('SO', 'Somalia'),
('SR', 'Suriname'),
('ST', 'Sao Tome and Principe'),
('SV', 'El Salvador'),
('SY', 'Syrian Arab Republic'),
('SZ', 'Swaziland'),
('TC', 'Turks and Caicos Islands'),
('TD', 'Chad'),
('TF', 'French Southern Territories'),
('TG', 'Togo'),
('TH', 'Thailand'),
('TJ', 'Tajikistan'),
('TK', 'Tokelau'),
('TL', 'Timor-Leste'),
('TM', 'Turkmenistan'),
('TN', 'Tunisia'),
('TO', 'Tonga'),
('TR', 'Turkey'),
('TT', 'Trinidad and Tobago'),
('TV', 'Tuvalu'),
('TW', 'Taiwan'),
('TZ', 'Tanzania, United Republic of'),
('UA', 'Ukraine'),
('UG', 'Uganda'),
('UM', 'United States Minor Outlying Islands'),
('US', 'United States'),
('UY', 'Uruguay'),
('UZ', 'Uzbekistan'),
('VA', 'Holy See (Vatican City State)'),
('VC', 'Saint Vincent and the Grenadines'),
('VE', 'Venezuela'),
('VG', 'Virgin Islands, British'),
('VI', 'Virgin Islands, U.s.'),
('VN', 'Viet Nam'),
('VU', 'Vanuatu'),
('WF', 'Wallis and Futuna'),
('WS', 'Samoa'),
('YE', 'Yemen'),
('YT', 'Mayotte'),
('ZA', 'South Africa'),
('ZM', 'Zambia'),
('ZW', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Table structure for table `hr_emp_pers_info`
--

CREATE TABLE `hr_emp_pers_info` (
  `id` int(11) NOT NULL,
  `employee_id` varchar(20) DEFAULT NULL,
  `fullname` varchar(100) NOT NULL,
  `dob` varchar(20) NOT NULL,
  `gender` char(1) NOT NULL,
  `residence` varchar(100) NOT NULL,
  `tel1` varchar(20) NOT NULL,
  `tel2` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `eme_name` varchar(50) NOT NULL,
  `eme_tel` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `hr_emp_pers_info`
--

INSERT INTO `hr_emp_pers_info` (`id`, `employee_id`, `fullname`, `dob`, `gender`, `residence`, `tel1`, `tel2`, `email`, `eme_name`, `eme_tel`) VALUES
(1, 'KAD/SYS/OO1', 'System Developer', '28-03-2017', 'N', 'Amasaman', '0541786220', '0245626487', 'marksbon@gmail.com', 'System Developer', '0541786220');

-- --------------------------------------------------------

--
-- Table structure for table `product_audit`
--

CREATE TABLE `product_audit` (
  `invoice_no` varchar(50) NOT NULL,
  `total_cost` int(11) NOT NULL,
  `sup_id` varchar(20) NOT NULL,
  `vendor` varchar(50) NOT NULL,
  `date_recv` varchar(50) NOT NULL,
  `comb_prod_id` text NOT NULL,
  `comb_unit_price` text NOT NULL,
  `comb_qty` text NOT NULL,
  `employee_id` varchar(20) NOT NULL,
  `comb_cost_p` varchar(255) NOT NULL,
  `comb_desc_id` varchar(255) NOT NULL,
  `comb_unit` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_category`
--

CREATE TABLE `product_category` (
  `cat_id` int(11) NOT NULL,
  `cat_name` varchar(50) NOT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_codes`
--

CREATE TABLE `product_codes` (
  `prod_id` int(11) NOT NULL,
  `prod_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_codes`
--

INSERT INTO `product_codes` (`prod_id`, `prod_name`) VALUES
(1, 'passport photo'),
(2, '2x2 usphoto'),
(3, 'Pc photo'),
(4, '4x6 photo'),
(5, '5x7 photo'),
(6, '5x5 photo'),
(7, 'Wallet photo'),
(8, '6x8 photo'),
(9, '5x10 photo'),
(10, '5x12 photo'),
(11, '5x 8.5'),
(12, '5x14'),
(13, '8x10 photo'),
(14, '8x8 photo'),
(15, '8x12 photo'),
(16, '8x14 photo'),
(17, '10x 12 photo'),
(18, '12x16'),
(19, '16x20 photo'),
(20, '20x24 photo'),
(21, '20x30 photo'),
(22, '24x30 photo'),
(23, '24x36 photo'),
(24, 'Print 10 or more'),
(25, '4x6 photo Print 10 or more'),
(26, '5x7 photo Print 10 or more'),
(27, '5x5 photo Print 10 or more'),
(28, '5x 8.5 photo'),
(29, '5x14 photo'),
(30, '12x16 photo'),
(31, '4x6 inch'),
(32, '5x7 inch'),
(33, '6x8 inch'),
(34, '8x10 inch'),
(35, '8x8 inch'),
(36, '5x 10 inch'),
(37, '5x12 inch'),
(38, '5x14 inch'),
(39, '5x 8.5 inch'),
(40, '8x12 inch'),
(41, '8x14 inch'),
(42, '10x 12 inch'),
(43, '12x16 inch'),
(44, '16x20 inch'),
(45, '20x24 inch'),
(46, '20x30 inch'),
(47, '24x30 inch'),
(48, '24x36 inch'),
(49, '16x20'),
(50, '20x24'),
(51, '20x30'),
(52, '24x36'),
(53, '10x28 glossy|mart'),
(54, '8x10 glossy|mart'),
(55, '12x16 glossy|mart'),
(56, 'Photoshoot(small)'),
(57, 'Photoshoot(Averatge)'),
(58, 'Photoshoot(semiLarge)'),
(59, 'Photoshoot(large)'),
(60, 'Photoshoot(medium)'),
(61, 'Photoshoot(semiMedium)'),
(62, 'Product shoot(small)'),
(63, 'Product shoot(Average)'),
(64, 'ProductShoot(medium)'),
(65, 'Product shoot(semiLarge)'),
(66, 'ProductShoot(large)'),
(67, 'Product shoot(semiMedium)'),
(68, 'UNPRINTED'),
(69, 'PRINTED');

-- --------------------------------------------------------

--
-- Table structure for table `product_customers`
--

CREATE TABLE `product_customers` (
  `id` int(11) NOT NULL,
  `fullname` varchar(50) NOT NULL,
  `mobile_number_1` varchar(20) NOT NULL,
  `mobile_number_2` varchar(20) NOT NULL,
  `address` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `location` varchar(100) NOT NULL,
  `date_created` datetime DEFAULT CURRENT_TIMESTAMP,
  `status` enum('active','inactive','deleted','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_desc`
--

CREATE TABLE `product_desc` (
  `desc_id` int(11) NOT NULL,
  `desc` varchar(50) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `status` enum('active','inactive','deleted','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_desc`
--

INSERT INTO `product_desc` (`desc_id`, `desc`, `cat_id`, `status`) VALUES
(18, 'Studio', 0, 'deleted'),
(19, 'Studio', 0, 'active'),
(20, 'Printing', 0, 'active'),
(21, 'Canvas OLD', 0, 'active'),
(22, 'Frame', 0, 'active'),
(23, 'Texturizing', 0, 'active'),
(24, 'Canvas NEW', 0, 'active'),
(25, 'Photobook', 0, 'active'),
(26, 'SOFTCOPY', 0, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `product_details`
--

CREATE TABLE `product_details` (
  `prod_id` varchar(20) NOT NULL,
  `product_id` varchar(20) DEFAULT NULL,
  `desc_id` int(20) NOT NULL,
  `unit_qty` int(5) NOT NULL,
  `cost_price` float DEFAULT NULL,
  `unit_price` float NOT NULL,
  `avail_qty` int(10) DEFAULT NULL,
  `expiry` varchar(20) DEFAULT NULL,
  `promo_qty` int(5) DEFAULT NULL,
  `promo_price` float DEFAULT NULL,
  `img_path` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_details`
--

INSERT INTO `product_details` (`prod_id`, `product_id`, `desc_id`, `unit_qty`, `cost_price`, `unit_price`, `avail_qty`, `expiry`, `promo_qty`, `promo_price`, `img_path`) VALUES
('39', 'PROD#0001', 19, 1, 15, 15, 1007, NULL, 0, 0, 'uploads/product/default_logo.png'),
('40', 'PROD#0002', 20, 1, 15, 15, 996, NULL, 0, 0, 'uploads/product/default_logo.png'),
('41', 'PROD#0003', 19, 1, 5, 5, 975, NULL, 0, 0, 'uploads/product/default_logo.png'),
('42', 'PROD#0004', 19, 1, 5, 5, 996, NULL, 0, 0, 'uploads/product/default_logo.png'),
('43', 'PROD#0005', 19, 1, 5, 5, 9997, NULL, 10, 3, 'uploads/product/default_logo.png'),
('44', 'PROD#0006', 19, 1, 8, 8, 957, NULL, 10, 2, 'uploads/product/default_logo.png'),
('1', 'PROD#0007', 19, 1, 20, 20, 959, NULL, 0, 0, 'uploads/product/default_logo.png'),
('2', 'PROD#0008', 19, 1, 20, 20, 999, NULL, 0, 0, 'uploads/product/default_logo.png'),
('3', 'PROD#0009', 19, 1, 4, 4, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('4', 'PROD#00010', 19, 5, 5, 5, 992, NULL, 0, 0, 'uploads/product/default_logo.png'),
('5', 'PROD#0011', 19, 1, 5, 5, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('6', 'PROD#0012', 19, 1, 5, 5, 998, NULL, 0, 0, 'uploads/product/default_logo.png'),
('7', 'PROD#0013', 19, 1, 8, 8, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('8', 'PROD#0014', 19, 1, 8, 8, 999, NULL, 0, 0, 'uploads/product/default_logo.png'),
('9', 'PROD#0015', 19, 1, 8, 8, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('10', 'PROD#0016', 19, 1, 8, 8, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('11', 'PROD#0017', 19, 1, 8, 8, 2000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('12', 'PROD#0018', 19, 1, 10, 10, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('13', 'PROD#0019', 19, 1, 10, 10, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('14', 'PROD#0020', 19, 1, 10, 10, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('15', 'PROD#0021', 19, 1, 12, 12, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('16', 'PROD#0022', 19, 1, 15, 15, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('17', 'PROD#0023', 19, 1, 30, 30, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('18', 'PROD#0024', 19, 1, 50, 50, 999, NULL, 0, 0, 'uploads/product/default_logo.png'),
('19', 'PROD#0025', 19, 1, 60, 60, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('20', 'PROD#0026', 19, 1, 80, 80, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('21', 'PROD#0027', 19, 1, 90, 90, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('22', 'PROD#0028', 19, 1, 100, 100, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('23', 'PROD#0029', 19, 1, 150, 150, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('1', 'PROD#0030', 20, 1, 10, 10, 959, NULL, 0, 0, NULL),
('2', 'PROD#0031', 20, 1, 15, 15, 999, NULL, 0, 0, NULL),
('24', 'PROD#0032', 20, 1, 4, 4, 1000, NULL, 10, 2, 'uploads/product/default_logo.png'),
('25', 'PROD#0033', 20, 1, 5, 5, 1000, NULL, 10, 5, 'uploads/product/default_logo.png'),
('26', 'PROD#0034', 20, 1, 5, 5, 1000, NULL, 10, 2, 'uploads/product/default_logo.png'),
('27', 'PROD#0035', 20, 1, 5, 5, 1000, NULL, 10, 2, 'uploads/product/default_logo.png'),
('7', 'PROD#0036', 20, 1, 8, 8, 1000, NULL, 0, 0, NULL),
('8', 'PROD#0037', 20, 1, 8, 8, 999, NULL, 0, 0, NULL),
('9', 'PROD#0038', 20, 1, 8, 8, 1000, NULL, 0, 0, NULL),
('10', 'PROD#0039', 20, 1, 8, 8, 1000, NULL, 0, 0, NULL),
('28', 'PROD#0040', 20, 1, 8, 8, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('29', 'PROD#0041', 20, 1, 10, 10, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('13', 'PROD#0042', 20, 1, 10, 10, 1000, NULL, 0, 0, NULL),
('14', 'PROD#0043', 20, 1, 10, 10, 1000, NULL, 0, 0, NULL),
('15', 'PROD#0044', 20, 1, 12, 12, 1000, NULL, 0, 0, NULL),
('16', 'PROD#0045', 20, 1, 15, 15, 1000, NULL, 0, 0, NULL),
('17', 'PROD#0046', 20, 1, 30, 30, 1000, NULL, 0, 0, NULL),
('30', 'PROD#0047', 20, 1, 50, 50, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('19', 'PROD#0048', 20, 1, 60, 60, 1000, NULL, 0, 0, NULL),
('20', 'PROD#0049', 20, 1, 80, 80, 1000, NULL, 0, 0, NULL),
('21', 'PROD#0050', 20, 1, 90, 90, 1000, NULL, 0, 0, NULL),
('22', 'PROD#0051', 20, 1, 100, 100, 1000, NULL, 0, 0, NULL),
('23', 'PROD#0052', 20, 1, 150, 150, 1000, NULL, 0, 0, NULL),
('31', 'PROD#0053', 22, 1, 12, 12, 999, NULL, 0, 0, 'uploads/product/default_logo.png'),
('31', 'PROD#0054', 22, 1, 15, 15, 999, NULL, 0, 0, NULL),
('32', 'PROD#0055', 22, 1, 18, 18, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('32', 'PROD#0056', 22, 1, 20, 20, 1000, NULL, 0, 0, NULL),
('33', 'PROD#0057', 22, 1, 20, 20, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('34', 'PROD#0058', 22, 1, 25, 25, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('35', 'PROD#0059', 22, 1, 25, 25, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('36', 'PROD#0060', 22, 1, 20, 20, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('37', 'PROD#0061', 22, 1, 20, 20, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('38', 'PROD#0062', 22, 1, 25, 25, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('39', 'PROD#0063', 22, 1, 20, 20, 1007, NULL, 0, 0, 'uploads/product/default_logo.png'),
('40', 'PROD#0064', 22, 1, 30, 30, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('40', 'PROD#0065', 22, 1, 35, 35, 1000, NULL, 0, 0, NULL),
('41', 'PROD#0066', 22, 1, 40, 40, 975, NULL, 0, 0, 'uploads/product/default_logo.png'),
('41', 'PROD#0067', 22, 1, 35, 35, 975, NULL, 0, 0, NULL),
('42', 'PROD#0068', 22, 1, 70, 70, 996, NULL, 0, 0, 'uploads/product/default_logo.png'),
('42', 'PROD#0069', 22, 1, 50, 50, 996, NULL, 0, 0, NULL),
('43', 'PROD#0070', 22, 1, 70, 70, 9997, NULL, 0, 0, 'uploads/product/default_logo.png'),
('43', 'PROD#0071', 22, 1, 100, 100, 9997, NULL, 0, 0, NULL),
('44', 'PROD#0072', 22, 1, 100, 100, 957, NULL, 0, 0, 'uploads/product/default_logo.png'),
('44', 'PROD#0073', 22, 1, 150, 150, 957, NULL, 0, 0, NULL),
('44', 'PROD#0074', 22, 1, 200, 200, 957, NULL, 0, 0, NULL),
('45', 'PROD#0075', 22, 1, 350, 350, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('45', 'PROD#0076', 22, 1, 300, 300, 1000, NULL, 0, 0, NULL),
('45', 'PROD#0077', 22, 1, 250, 250, 1000, NULL, 0, 0, NULL),
('46', 'PROD#0078', 22, 1, 450, 450, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('46', 'PROD#0079', 22, 1, 350, 350, 1000, NULL, 0, 0, NULL),
('47', 'PROD#0080', 22, 1, 500, 500, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('47', 'PROD#0081', 22, 1, 450, 450, 1000, NULL, 0, 0, NULL),
('48', 'PROD#0082', 22, 1, 500, 500, 1000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('48', 'PROD#0083', 22, 1, 600, 600, 1000, NULL, 0, 0, NULL),
('31', 'PROD#0084', 23, 1, 20, 20, 999, NULL, 0, 0, NULL),
('32', 'PROD#0085', 23, 1, 25, 25, 1000, NULL, 0, 0, NULL),
('33', 'PROD#0086', 23, 1, 30, 30, 1000, NULL, 0, 0, NULL),
('34', 'PROD#0087', 23, 1, 40, 40, 1000, NULL, 0, 0, NULL),
('35', 'PROD#0088', 23, 1, 40, 40, 1000, NULL, 0, 0, NULL),
('36', 'PROD#0089', 23, 1, 30, 30, 1000, NULL, 0, 0, NULL),
('10', 'PROD#0090', 23, 1, 30, 30, 1000, NULL, 0, 0, NULL),
('38', 'PROD#0091', 23, 1, 30, 30, 1000, NULL, 0, 0, NULL),
('39', 'PROD#0092', 23, 1, 25, 25, 1007, NULL, 0, 0, NULL),
('40', 'PROD#0093', 23, 1, 35, 35, 1000, NULL, 0, 0, NULL),
('41', 'PROD#0094', 23, 1, 40, 40, 975, NULL, 0, 0, NULL),
('42', 'PROD#0095', 23, 1, 60, 60, 996, NULL, 0, 0, NULL),
('43', 'PROD#0096', 23, 1, 80, 80, 9997, NULL, 0, 0, NULL),
('44', 'PROD#0097', 23, 1, 100, 100, 957, NULL, 0, 0, NULL),
('45', 'PROD#0098', 23, 1, 150, 150, 1000, NULL, 0, 0, NULL),
('46', 'PROD#0099', 23, 1, 200, 200, 1000, NULL, 0, 0, NULL),
('47', 'PROD#00100', 23, 1, 250, 250, 1000, NULL, 0, 0, NULL),
('48', 'PROD#101', 23, 1, 300, 300, 1000, NULL, 0, 0, NULL),
('18', 'PROD#102', 21, 1, 200, 200, 999, NULL, 0, 0, NULL),
('49', 'PROD#103', 21, 1, 250, 250, 998, NULL, 0, 0, 'uploads/product/default_logo.png'),
('50', 'PROD#104', 21, 1, 400, 400, 996, NULL, 0, 0, 'uploads/product/default_logo.png'),
('51', 'PROD#105', 21, 1, 450, 450, 999, NULL, 0, 0, 'uploads/product/default_logo.png'),
('52', 'PROD#106', 21, 1, 550, 550, 998, NULL, 0, 0, 'uploads/product/default_logo.png'),
('18', 'PROD#107', 24, 1, 250, 250, 999, NULL, 0, 0, NULL),
('49', 'PROD#108', 24, 1, 300, 300, 998, NULL, 0, 0, NULL),
('50', 'PROD#109', 24, 1, 450, 450, 996, NULL, 0, 0, NULL),
('51', 'PROD#110', 24, 1, 500, 500, 999, NULL, 0, 0, NULL),
('52', 'PROD#111', 24, 1, 600, 600, 998, NULL, 0, 0, NULL),
('53', 'PROD#112', 25, 1, 900, 900, 2000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('54', 'PROD#113', 25, 1, 600, 600, 2000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('55', 'PROD#114', 25, 1, 1200, 1200, 2000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('56', 'PROD#115', 20, 1, 150, 150, 2000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('57', 'PROD#116', 20, 1, 200, 200, 2000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('58', 'PROD#117', 20, 1, 300, 300, 2000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('59', 'PROD#118', 20, 1, 400, 400, 2000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('56', 'PROD#119', 19, 1, 60, 60, 2000, NULL, 0, 0, NULL),
('57', 'PROD#120', 19, 1, 100, 100, 2000, NULL, 0, 0, NULL),
('60', 'PROD#121', 19, 1, 200, 200, 2000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('58', 'PROD#122', 19, 1, 300, 300, 2000, NULL, 0, 0, NULL),
('59', 'PROD#123', 19, 1, 400, 400, 2000, NULL, 0, 0, NULL),
('61', 'PROD#124', 19, 1, 150, 150, 1999, NULL, 0, 0, 'uploads/product/default_logo.png'),
('62', 'PROD#125', 19, 1, 60, 60, 2000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('63', 'PROD#126', 19, 1, 100, 100, 2000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('64', 'PROD#127', 19, 1, 200, 200, 2000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('65', 'PROD#128', 19, 1, 300, 300, 2000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('66', 'PROD#129', 19, 1, 400, 400, 2000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('61', 'PROD#130', 19, 1, 150, 150, 1999, NULL, 0, 0, NULL),
('67', 'PROD#131', 19, 1, 150, 150, 2000, NULL, 0, 0, 'uploads/product/default_logo.png'),
('68', 'PROD#132', 26, 1, 0, 5, 998, NULL, 0, 0, 'uploads/product/default_logo.png'),
('69', 'PROD#133', 26, 1, 0, 2, 1000000, NULL, 0, 0, 'uploads/product/default_logo.png');

-- --------------------------------------------------------

--
-- Table structure for table `product_suppliers`
--

CREATE TABLE `product_suppliers` (
  `sup_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `tel1` varchar(20) NOT NULL,
  `tel2` varchar(20) DEFAULT NULL,
  `addr` varchar(255) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `loc` varchar(50) NOT NULL,
  `pay_type` varchar(20) DEFAULT NULL,
  `prod_type` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `product_suppliers_accounts`
--

CREATE TABLE `product_suppliers_accounts` (
  `id` int(11) NOT NULL,
  `sup_id` int(11) NOT NULL,
  `bank` varchar(50) NOT NULL,
  `branch` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `num` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_transact`
--

CREATE TABLE `product_transact` (
  `id` int(11) NOT NULL,
  `description` varchar(100) NOT NULL,
  `prod_id` varchar(50) NOT NULL,
  `qty_withdrawn` varchar(50) NOT NULL,
  `unit_price` varchar(50) NOT NULL,
  `tot_cost` int(11) NOT NULL,
  `date_withdrawn` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `employee_id` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `report_dailyledger`
--
CREATE TABLE `report_dailyledger` (
`Autoid` int(11)
,`Description` varchar(100)
,`CostPrice` float
,`Qty_Bought` varchar(50)
,`SellingPrice` float
,`T_Cost` double
,`Tax` double
);

-- --------------------------------------------------------

--
-- Table structure for table `report_purchaseorder`
--

CREATE TABLE `report_purchaseorder` (
  `id` int(11) NOT NULL,
  `sup_id` int(11) NOT NULL,
  `invoice` int(5) NOT NULL,
  `tot_cost` float NOT NULL,
  `products` text NOT NULL,
  `quantity` text NOT NULL,
  `prices` text NOT NULL,
  `remarks` varchar(255) NOT NULL,
  `pay` float NOT NULL,
  `bal` float NOT NULL,
  `date_created` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reserved_license_keys`
--

CREATE TABLE `reserved_license_keys` (
  `license_id` int(20) NOT NULL,
  `key` varchar(100) NOT NULL,
  `end_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reserved_registered_keys`
--

CREATE TABLE `reserved_registered_keys` (
  `reg_key_id` int(20) NOT NULL,
  `license_id` int(20) NOT NULL,
  `client_id` int(11) NOT NULL,
  `state` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stores`
--

CREATE TABLE `stores` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `location` varchar(255) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stores`
--

INSERT INTO `stores` (`id`, `name`, `location`, `date_created`) VALUES
(1, 'Accra Mall Station', 'Inside The Mall', '2018-01-25 18:53:04'),
(2, 'Achimota Retail Center', 'Inside The Mall', '2018-01-25 18:53:04'),
(3, 'Junction Mall', 'Inside The Mall', '2018-01-25 18:53:04'),
(4, 'West Hills Mall', 'Inside The Mall', '2018-01-25 18:53:04');

-- --------------------------------------------------------

--
-- Stand-in structure for view `suppliers_full_details`
--
CREATE TABLE `suppliers_full_details` (
`sup_id` int(11)
,`name` varchar(255)
,`addr` varchar(255)
,`loc` varchar(50)
,`tel1` varchar(20)
,`tel2` varchar(20)
,`email` varchar(100)
,`prod_type` text
,`pay_type` varchar(20)
,`bank` varchar(50)
,`branch` varchar(50)
,`acctname` varchar(100)
,`num` varchar(25)
);

-- --------------------------------------------------------

--
-- Table structure for table `tax_system`
--

CREATE TABLE `tax_system` (
  `id` int(11) NOT NULL,
  `percentage` double NOT NULL,
  `status` enum('active','inactive','deleted') NOT NULL DEFAULT 'active',
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tax_system`
--

INSERT INTO `tax_system` (`id`, `percentage`, `status`, `date_created`) VALUES
(1, 19.5, 'active', '2018-01-16 11:44:20');

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_account_details`
--
CREATE TABLE `v_account_details` (
`Id` int(11)
,`Name` varchar(50)
,`Refcon` varchar(20)
,`Descrip` varchar(255)
,`Debit_Amt` int(11)
,`Credit_Amt` int(11)
,`Balance` bigint(12)
);

-- --------------------------------------------------------

--
-- Structure for view `access_user_details`
--
DROP TABLE IF EXISTS `access_user_details`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `access_user_details`  AS  select `access_users`.`id` AS `id`,`access_users`.`username` AS `Username`,`access_users`.`passwd` AS `Password`,`access_users`.`active` AS `Account_Status`,`hr_emp_pers_info`.`employee_id` AS `Employee_id`,`access_roles_priviledges_user`.`roles` AS `User_Roles`,`access_roles_priviledges_user`.`priviledges` AS `User_Priviledges`,`access_roles_priviledges_user`.`group_id` AS `Group_ID`,`access_roles_priviledges_group`.`group_name` AS `Group_Name`,`access_roles_priviledges_group`.`roles` AS `Group_Roles`,`access_roles_priviledges_group`.`priviledges` AS `Group_Priviledges` from (((`access_users` left join `hr_emp_pers_info` on((`hr_emp_pers_info`.`id` = `access_users`.`id`))) left join `access_roles_priviledges_user` on((`access_roles_priviledges_user`.`employee_id` = `hr_emp_pers_info`.`id`))) left join `access_roles_priviledges_group` on((`access_roles_priviledges_group`.`group_id` = `access_roles_priviledges_user`.`group_id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `full_product_details`
--
DROP TABLE IF EXISTS `full_product_details`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `full_product_details`  AS  select `product_codes`.`prod_name` AS `Item_Name`,`product_details`.`product_id` AS `ProductCode`,`product_desc`.`desc` AS `Description`,`product_details`.`unit_qty` AS `Unit_Qty`,`product_details`.`unit_price` AS `Unit_Price`,`product_details`.`promo_qty` AS `Promo_Qty`,`product_details`.`promo_price` AS `Promo_Price`,`product_details`.`avail_qty` AS `Current_Qty`,`product_details`.`expiry` AS `Expiry`,`product_codes`.`prod_id` AS `Product_ID`,`product_desc`.`desc_id` AS `DescriptionID` from ((`product_codes` join `product_details` on((`product_details`.`prod_id` = `product_codes`.`prod_id`))) join `product_desc` on((`product_desc`.`desc_id` = `product_details`.`desc_id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `report_dailyledger`
--
DROP TABLE IF EXISTS `report_dailyledger`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `report_dailyledger`  AS  (select `product_transact`.`id` AS `Autoid`,`product_transact`.`description` AS `Description`,`product_details`.`cost_price` AS `CostPrice`,`product_transact`.`qty_withdrawn` AS `Qty_Bought`,`product_details`.`unit_price` AS `SellingPrice`,(`product_details`.`unit_price` * `product_transact`.`qty_withdrawn`) AS `T_Cost`,((`product_details`.`unit_price` * `product_transact`.`qty_withdrawn`) * 0.175) AS `Tax` from (`product_transact` join `product_details` on((`product_details`.`prod_id` = `product_transact`.`prod_id`)))) ;

-- --------------------------------------------------------

--
-- Structure for view `suppliers_full_details`
--
DROP TABLE IF EXISTS `suppliers_full_details`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `suppliers_full_details`  AS  (select `product_suppliers`.`sup_id` AS `sup_id`,`product_suppliers`.`name` AS `name`,`product_suppliers`.`addr` AS `addr`,`product_suppliers`.`loc` AS `loc`,`product_suppliers`.`tel1` AS `tel1`,`product_suppliers`.`tel2` AS `tel2`,`product_suppliers`.`email` AS `email`,`product_suppliers`.`prod_type` AS `prod_type`,`product_suppliers`.`pay_type` AS `pay_type`,`product_suppliers_accounts`.`bank` AS `bank`,`product_suppliers_accounts`.`branch` AS `branch`,`product_suppliers_accounts`.`name` AS `acctname`,`product_suppliers_accounts`.`num` AS `num` from (`product_suppliers` left join `product_suppliers_accounts` on((`product_suppliers_accounts`.`sup_id` = `product_suppliers`.`sup_id`)))) ;

-- --------------------------------------------------------

--
-- Structure for view `v_account_details`
--
DROP TABLE IF EXISTS `v_account_details`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_account_details`  AS  select `account_types`.`id` AS `Id`,`account_types`.`acc_name` AS `Name`,`account_types`.`acc_ref_no` AS `Refcon`,`account_types`.`acc_desc` AS `Descrip`,`accounts_transactions`.`debit_amt` AS `Debit_Amt`,`accounts_transactions`.`credit_amt` AS `Credit_Amt`,(`accounts_transactions`.`credit_amt` - `accounts_transactions`.`debit_amt`) AS `Balance` from (`account_types` left join `accounts_transactions` on((`accounts_transactions`.`acc_type_id` = `account_types`.`id`))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access_login_failed`
--
ALTER TABLE `access_login_failed`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `access_login_success`
--
ALTER TABLE `access_login_success`
  ADD PRIMARY KEY (`login_id`);

--
-- Indexes for table `access_roles_priviledges_group`
--
ALTER TABLE `access_roles_priviledges_group`
  ADD PRIMARY KEY (`group_id`),
  ADD UNIQUE KEY `name` (`group_name`);

--
-- Indexes for table `access_roles_priviledges_user`
--
ALTER TABLE `access_roles_priviledges_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `employee_id_UNIQUE` (`employee_id`);

--
-- Indexes for table `access_users`
--
ALTER TABLE `access_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username_UNIQUE` (`username`);

--
-- Indexes for table `account_types`
--
ALTER TABLE `account_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `accounts_transactions`
--
ALTER TABLE `accounts_transactions`
  ADD PRIMARY KEY (`auto_id`);

--
-- Indexes for table `dashboard_tabs`
--
ALTER TABLE `dashboard_tabs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_last_ids`
--
ALTER TABLE `general_last_ids`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group_roles_priviledges`
--
ALTER TABLE `group_roles_priviledges`
  ADD PRIMARY KEY (`group_id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `hr_companyinfo`
--
ALTER TABLE `hr_companyinfo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hr_countries`
--
ALTER TABLE `hr_countries`
  ADD PRIMARY KEY (`cou_code`);

--
-- Indexes for table `hr_emp_pers_info`
--
ALTER TABLE `hr_emp_pers_info`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQUE` (`employee_id`,`tel1`);

--
-- Indexes for table `product_category`
--
ALTER TABLE `product_category`
  ADD PRIMARY KEY (`cat_id`),
  ADD UNIQUE KEY `cat_name_UNIQUE` (`cat_name`);

--
-- Indexes for table `product_codes`
--
ALTER TABLE `product_codes`
  ADD PRIMARY KEY (`prod_id`),
  ADD UNIQUE KEY `prod_id_UNIQUE` (`prod_id`);

--
-- Indexes for table `product_customers`
--
ALTER TABLE `product_customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_desc`
--
ALTER TABLE `product_desc`
  ADD PRIMARY KEY (`desc_id`);

--
-- Indexes for table `product_details`
--
ALTER TABLE `product_details`
  ADD UNIQUE KEY `UNIQUE` (`product_id`);

--
-- Indexes for table `product_suppliers`
--
ALTER TABLE `product_suppliers`
  ADD PRIMARY KEY (`sup_id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `product_suppliers_accounts`
--
ALTER TABLE `product_suppliers_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_transact`
--
ALTER TABLE `product_transact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `report_purchaseorder`
--
ALTER TABLE `report_purchaseorder`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reserved_license_keys`
--
ALTER TABLE `reserved_license_keys`
  ADD PRIMARY KEY (`license_id`,`key`,`end_date`),
  ADD UNIQUE KEY `UNIQUE` (`key`);

--
-- Indexes for table `reserved_registered_keys`
--
ALTER TABLE `reserved_registered_keys`
  ADD PRIMARY KEY (`reg_key_id`),
  ADD UNIQUE KEY `UNIQUE` (`license_id`,`client_id`);

--
-- Indexes for table `stores`
--
ALTER TABLE `stores`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tax_system`
--
ALTER TABLE `tax_system`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `access_login_failed`
--
ALTER TABLE `access_login_failed`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `access_login_success`
--
ALTER TABLE `access_login_success`
  MODIFY `login_id` int(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `access_roles_priviledges_group`
--
ALTER TABLE `access_roles_priviledges_group`
  MODIFY `group_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `access_roles_priviledges_user`
--
ALTER TABLE `access_roles_priviledges_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `access_users`
--
ALTER TABLE `access_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `account_types`
--
ALTER TABLE `account_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `accounts_transactions`
--
ALTER TABLE `accounts_transactions`
  MODIFY `auto_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `dashboard_tabs`
--
ALTER TABLE `dashboard_tabs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `general_last_ids`
--
ALTER TABLE `general_last_ids`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `hr_companyinfo`
--
ALTER TABLE `hr_companyinfo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `hr_emp_pers_info`
--
ALTER TABLE `hr_emp_pers_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `product_category`
--
ALTER TABLE `product_category`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_codes`
--
ALTER TABLE `product_codes`
  MODIFY `prod_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;
--
-- AUTO_INCREMENT for table `product_customers`
--
ALTER TABLE `product_customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_desc`
--
ALTER TABLE `product_desc`
  MODIFY `desc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `product_suppliers`
--
ALTER TABLE `product_suppliers`
  MODIFY `sup_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_suppliers_accounts`
--
ALTER TABLE `product_suppliers_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_transact`
--
ALTER TABLE `product_transact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `report_purchaseorder`
--
ALTER TABLE `report_purchaseorder`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `reserved_license_keys`
--
ALTER TABLE `reserved_license_keys`
  MODIFY `license_id` int(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `reserved_registered_keys`
--
ALTER TABLE `reserved_registered_keys`
  MODIFY `reg_key_id` int(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `stores`
--
ALTER TABLE `stores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tax_system`
--
ALTER TABLE `tax_system`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
