<div class="col-md-12">
  <div class="card">
    <div class="card-header">
      <form action="<?=base_url()?>report/retrieve_record" method="post">
        <div class="col-md-3">
          <label class="col-md-12 control-label">Select Store</label>
          <select class="form-control" name="store" placeholder="--- Select One ---" style="width:100% !important" value="" required>
            <option label="--- Select One ---"></option>
              <?php foreach(STORES as $key => $store) : ?>
            <option><?=$key?></option>
              <?php endforeach; ?>
            <!--<option value="Achimota Retail Center">Achimota Mall</option>
            <option>Junction Mall</option>
            <option>West Hills Mall</option>
            <option>Kumasi Mall</option>-->
          </select>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label class="col-md-6 control-label">Account</label>
            <select class=" form-control" name="account" placeholder="" style="width:100% !important" required>
              <option label="--- Select One ---"></option>
              <option value="dailysales"> Daily Sales </option>
              <?php /*
                if(!empty($accounts)) :
                  foreach($accounts As $acc) :
                    print "<option value='".$acc->id."'>".ucwords($acc->acc_name)."</option>";
                  endforeach;
                endif;*/
              ?>
            </select>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label class="col-md-6 control-label">Duration</label>
            <div class="col-md-12">
              <input type="text" class="form-control datepicker" placeholder="Date" name="date" required style="height:33px;">
            </div>
          </div> 
        </div>
        <div class="col-md-2">
          <div class="form-group">
            <label class="col-md-6 control-label"></label>
            <div class="col-md-12">
              <button type="submit" name="gen_report" class="btn btn-sm btn-warning" style="margin-top:15px !important;" >Generate</button>
            </div>
          </div>
        </div>
        <hr>
      </form>
    </div>
    <div class="card-body">
      
        <?php if(!empty($_SESSION['dailyreport'])) :  $dailyreport = $_SESSION['dailyreport'];
          unset($_SESSION['dailyreport']);
        ?> 
        <table class="datatable table table-striped primary" cellspacing="0" width="100%">
          <thead>
        
            <tr>
              <th>ID</th>
              <th>Store Name</th>
              <th>Products</th>
              <th>Description</th>
              <th>Sub Total</th>
              <th>Date</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <?php  if(!empty($dailyreport)) : $counter = 1; foreach($dailyreport As $stores) : foreach($stores As $report) : ?>
            <tr>
              <td><?= $counter ?></td>
              <td><?= $report->store ?></td>
              <?php 
                $prods = explode('|',$report->prod_id);

                foreach($prods As $prod)
                {
                  $data = [ 'prod_id' =>  $prod ];
                  $product = $this->Universal_Retrieval->ret_data_with_s_cond_row('product_codes','prod_id',$data);
                  
                  if(!empty($product)){
                    @$productname .= $product->prod_name. ",";
                  }
                  
                }
              ?>

              <td><?php print substr(@$productname, 0, 30); unset($productname); ?></td>
              <td><?= $report->description ?></td>
              <td><?= $report->tot_cost ?></td>
              <td><?= $report->date_withdrawn ?></td>
              <td><button class='btn btn-success btn-xs salesdetails' onclick="viewOrderDetails(this)" data-trans="<?= $report->id ?>" data-resource="<?=$report->store?>"><i class='fa fa-lock'></i> Details</button>
              </td>
            </tr>
            <?php $counter++; endforeach; endforeach; endif; ?>
          </tbody>
        </table> 
        <div class="row"> 
          <div class="col-md-3"></div>
          <div class="col-md-2"></div>
          <div class="col-md-6">
            <p class="pull-left"><b>Total:</b> GHȻ <b class="checkoutamt" style="font-size: 40px"><?=$_SESSION['report']['total']?></b> </p>
          </div>
          <div class="col-md-1"></div>
        </div>
        <?php endif; ?>
      
    </div>
  </div>
</div>