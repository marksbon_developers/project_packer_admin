<nav class="navbar navbar-default" id="navbar">
  <div class="container-fluid">
    <div class="navbar-collapse collapse in">
      <ul class="nav navbar-nav navbar-mobile">
        <li>
          <button type="button" class="sidebar-toggle">
            <i class="fa fa-bars"></i>
          </button>
        </li>
        <li class="logo">
          <a class="navbar-brand" href="#"><span class="highlight">PARK</span> ER</a>
        </li>
        <li>
          <button type="button" class="navbar-toggle">
            <img class="profile-img" src="<?= base_url() ?>resources/images/profile.png">
          </button>
        </li>
      </ul>
      <ul class="nav navbar-nav navbar-left">
        <li class="navbar-title"  id="txt"><i class="fa fa-clock"></i></li>
       <div class="title" style="font-size:25px ">Administrator</div>
        <li>
          <?php if(!empty($_SESSION['success'])) : ?>
            <div class="col-md-12 col-sm-12">
              <div class="alert alert-success alert-dismissible" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                  <strong><?= $this->session->flashdata("success") ?></strong> 
              </div>
            </div>
          <?php elseif (!empty($_SESSION['error'])) : ?>
            <div class="col-md-12 col-sm-12">
              <div class="alert alert-danger alert-dismissible" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                  <strong><?= $this->session->flashdata("error") ?></strong> 
              </div>
            </div>
          <?php endif; ?>
        </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
       <!-- <li class="dropdown notification danger">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <div class="icon"><i class="fa fa-home" aria-hidden="true"></i></div>
            <div class="title">Switch Stores</div>
          </a>
          <div class="dropdown-menu">
            <ul>
              <li class="dropdown-header">Switch Stores</li>
              <li >
                <a href="#">
                  <span class="badge badge-danger pull-right">AM</span>
                  <div class="message">
                    <div class="content">
                      <div class="title">Accra Mall</div>
                    </div>
                  </div>
                </a>
              </li>
              <li >
                <a href="#">
                  <span class="badge badge-danger pull-right">ACM</span>
                  <div class="message">
                    <div class="content">
                      <div class="title">Ahcimota Mall</div>
                    </div>
                  </div>
                </a>
              </li>
              <li >
                <a href="#">
                  <span class="badge badge-danger pull-right">WM</span>
                  <div class="message">
                    <div class="content">
                      <div class="title">WestHills Mall</div>
                    </div>
                  </div>
                </a>
              </li>
              <li >
                <a href="#">
                  <span class="badge badge-danger pull-right">WM</span>
                  <div class="message">
                    <div class="content">
                      <div class="title">Junction Mall</div>
                    </div>
                  </div>
                </a>
              </li>
              <li class="dropdown-empty">
                Click to Switch Stores
              </li>
              <li class="dropdown-footer">
              </li>
            </ul>
          </div>
        </li>
     <!--   <li class="dropdown notification warning">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <div class="icon"><i class="fa fa-comments" aria-hidden="true"></i></div>
            <div class="title">Unread Messages</div>
            <div class="count">0</div>
          </a>
          <div class="dropdown-menu">
            <ul>
              <li class="dropdown-header">Message</li>
              <li>
                <a href="#">
                  <span class="badge badge-warning pull-right">0</span>
                  <div class="message">
                    <img class="profile" src="">
                    <div class="content">
                      <div class="title"></div>
                      <div class="description"></div>
                    </div>
                  </div>
                </a>
              </li>
              <li class="dropdown-footer">
                <a href="#">View All <i class="fa fa-angle-right" aria-hidden="true"></i></a>
              </li>
            </ul>
          </div>
        </li>-->
     <!--   <li class="dropdown notification danger">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <div class="icon"><i class="fa fa-bell" aria-hidden="true"></i></div>
            <div class="title">System Notifications</div>
            <div class="count">0</div>
          </a>
          <div class="dropdown-menu">
            <ul>
              <li class="dropdown-header">Notification</li>
              <li>
                
              </li>
              <li>
                <a href="#">
                  <span class="badge badge-danger pull-right">0</span>
                  Inbox
                </a>
              </li>
              <li>
                <a href="#">
                  <span class="badge badge-danger pull-right">5</span>
                  Issues Report
                </a>
              </li>
              <li class="dropdown-footer">
                <a href="#">View All <i class="fa fa-angle-right" aria-hidden="true"></i></a>
              </li>
            </ul>
          </div>
        </li>-->
        <li class="dropdown profile">
          <a href="/html/pages/profile.html" class="dropdown-toggle"  data-toggle="dropdown">
            <img class="profile-img" src="<?= base_url()?>resources/images/profile.png"/>
            <div class="title">Profile</div>
          </a>
          <div class="dropdown-menu">
            <div class="profile-info">
              <h4 class="username"><?= $_SESSION['fullname'] ?></h4>
            </div>
            <ul class="action">
              <li>
                <a href="#">
                 Profile
                </a>
              </li>
              <li>
                <a href="<?= base_url()?>Access/Logout">
                  Logout
                </a>
              </li>
            </ul>
          </div>
        </li>
      </ul>
    </div>
  </div>
</nav>