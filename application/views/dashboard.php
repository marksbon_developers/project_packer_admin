<div class="row">
  <?php /*if(!empty($allstores_data)) : $counter = 1; foreach($allstores_data as $shop=>$value) : ?>
    <div class="col-md-3">
      <!-- Invitation stats white -->
      <div class="panel text-center">
        <div class="panel-body">
          <div class="title" style="font-weight:600"><?=strtoupper($shop)?></div>
          <div class="text-muted content-group">Daily staticstis</div>
          <div class=" col-lg-12 col-sm-12">
            <canvas id="piechart<?=$counter?>" width="100" height=""></canvas>
          </div>
          <div style="margin-left: -40px;margin-right: -40px;display:none">
           <ul style="list-style-type:none;margin-right: 40px">
              <?php if(!empty($value['all_products_sold'])) : foreach($value['all_products_sold'] as $product=>$qty) : ?>
              <li data-slice="0" style="border-bottom: solid 2px #29B6F6"><?=$product?>:<span ><?=$qty?></span></li>
              <?php endforeach; endif; ?>
            </ul>
          </div>
        </div>
        <div class="panel-body panel-body-accent pb-15">
          <div class="row">
            <div class="col-xs-4">
              <a href="<?=base_url()?>dashboard/salesreport">
               <div class="text-uppercase text-size-mini text-muted"> Revenue</div>
                <div class="value"><span class="sign">Ȼ</span><?php (@$value['total_revenue']) ? print number_format($value['total_revenue'],2) : print "0"; ?></div>
              </a>
            </div>
            <div class="col-xs-4">
              <div class="text-uppercase text-size-mini text-muted">Expences</div>
              <div class="value"><span class="sign">Ȼ</span><?php (@$value['total_tax']) ? print number_format($value['total_tax'],2) : print "0"; ?></div>
            </div>
            <div class="col-xs-4">
              <div class="text-uppercase text-size-mini text-muted">Sold </div>
              <div class="value"><span class="sign"></span><?php (@$value['total_items_sold']) ? print number_format($value['total_items_sold']) : print "0"; ?></div>
            </div>
          </div>
        </div>
      </div>
      <!-- /invitation stats white -->
    </div>
  <?php $counter++; endforeach; endif; */?>
</div>

<div class="row">
  <div class="col-lg-12 col-md-6 col-sm-12 col-xs-12">
    <div class="card card-tab card-mini">
      <div class="card-header">
        <ul class="nav nav-tabs tab-stats">
          <li role="accra" class="active">
            <a href="#dailystat" aria-controls="tab1" role="tab" data-toggle="tab">DAILY STATISTICS</a>
          </li>
          <li role="westhills">
            <a href="#monthly" aria-controls="tab2" role="tab" data-toggle="tab">MONTHLY SUMMARY</a>
          </li>
          <li role="tab2">
            <a href="#graphicaldata" aria-controls="tab3" role="tab" data-toggle="tab">PRODUCTS SUMMARY</a>
          </li>
        </ul>
      </div>
      <div class="card-body tab-content">
        <div role="tabpanel" class="tab-pane active" id="dailystat">
          <div class="row">
            <?php if(!empty($allstores_data)) : $counter = 1; foreach($allstores_data as $shop=>$value) : ?>
              <div class="col-md-3">
                <h4><strong><?=$value['date']?></strong></h4>
                <!-- Invitation stats white -->
                <div class="panel text-center">
                  <div class="panel-body">
                    <div class="title" style="font-weight:600"><?=strtoupper($shop)?></div>
                    <div class="text-muted content-group">Daily staticstis</div>
                    <div class=" col-lg-12 col-sm-12">
                      <canvas id="piechart<?=$counter?>" width="100" height=""></canvas>
                    </div>
                    <div style="margin-left: -40px;margin-right: -40px;display:none">
                     <ul style="list-style-type:none;margin-right: 40px">
                        <?php if(!empty($value['all_products_sold'])) : foreach($value['all_products_sold'] as $product=>$qty) : ?>
                        <li data-slice="0" style="border-bottom: solid 2px #29B6F6"><?=$product?>:<span ><?=$qty?></span></li>
                        <?php endforeach; endif; ?>
                      </ul>
                    </div>
                  </div>
                  <div class="panel-body panel-body-accent pb-15">
                    <div class="row">
                      <div class="col-xs-4">
                        <a href="<?=base_url()?>dashboard/salesreport">
                         <div class="text-uppercase text-size-mini text-muted"> Revenue</div>
                          <div class="value"><span class="sign">Ȼ</span><?php (@$value['total_revenue']) ? print number_format($value['total_revenue'],2) : print "0"; ?></div>
                        </a>
                      </div><!-- 
                      <div class="col-xs-4">
                        <div class="text-uppercase text-size-mini text-muted">Expences</div>
                        <div class="value"><span class="sign">Ȼ</span><?php (@$value['total_tax']) ? print number_format($value['total_tax'],2) : print "0"; ?></div>
                      </div> -->
                      <div class="col-xs-8">
                        <div class="text-uppercase text-size-mini text-muted">Items Sold </div>
                        <div class="value"><span class="sign"></span><?php (@$value['total_items_sold']) ? print number_format($value['total_items_sold']) : print "0"; ?></div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- /invitation stats white -->
              </div>
            <?php $counter++; endforeach; endif; ?>
          </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="monthly">
          <div class="row">
            <table class="table card-table table-bordred table-xs" id="monthly_stats">
              <thead>
                <tr style="font-weight: 600">
                  <th>Month</th>
                  <th class="right">MALL NAME</th>
                  <th>Revenue (GHȻ)</th>
                  <th>Highest Sold</th>
                  <th>Least Sold</th>
                </tr>
              </thead>
              <tbody>
                <?php if(!empty($monthly_stat)) : foreach ($monthly_stat as $month => $info_array) : ?>
                <tr>
                  <td rowspan="<?=NO_OF_STORES?>"><strong><?=$month?></strong></td>
                  <?php foreach ($info_array as $store_name => $store_info) : ?>
                  <td><?=$store_name?></td>
                  <td><strong><?="Ȼ".number_format($store_info['revenue'],2)?></strong></td>
                  <td><?=@$store_info['highest']?></td>
                  <td><?=@$store_info['least']?></td>
                </tr>
                <?php endforeach; ?>
              <?php endforeach; endif; ?>
              </tbody>
            </table>
          </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="graphicaldata">
          <div class="row">
              <div class="col-lg-06">
                  <table class="table card-table table-bordred table-xs">
                      <thead>
                          <tr style="font-weight: 600">
                              <th>Shop Name</th>
                              <th class="right">Categories NAME</th>
                              <th>Cash Representation (GHȻ)</th>
                              <th>Total NetWorth (GHȻ)</th>
                          </tr>
                      </thead>
                      <tbody>
                      <?php if(!empty($shopProductStatistics)) : foreach ($shopProductStatistics as $shopname => $shopdata) : $numOfCategory = sizeof($shopdata['data']); ?>
                          <tr>
                              <td rowspan="<?=$numOfCategory?>" style=" padding-top: 12%;"><strong><?=$shopname?></strong></td>
                              <?php foreach($shopdata['data'] as $key => $value) : ?>
                              <td><?=$key?></td>
                              <td><?=number_format($value, 2)?></td>
                              <?php array_shift($shopdata['data']); break; endforeach; ?>
                              <td rowspan="<?=$numOfCategory?>" style=" padding-top: 12%;"><strong><?=$shopdata['netWorth']?></strong></td>
                          </tr>
                          <?php foreach($shopdata['data'] as $key => $value) :
                                //if($value === end($shopdata['data']))
                          ?>
                          <tr>
                              <td><?=$key?></td>
                              <td><?=number_format($value, 2)?></td>
                          </tr>
                          <?php endforeach;?>
                      <?php endforeach; endif; ?>
                      </tbody>
                  </table>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
