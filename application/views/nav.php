<aside class="app-sidebar" id="sidebar">
  <div class="sidebar-header">
    <a class="sidebar-brand" href="#"><img src="<?= base_url() ?>resources/images/favicon.png"></a>
    <button type="button" class="sidebar-toggle">
      <i class="fa fa-times"></i>
    </button>
  </div>
  <div class="sidebar-menu">
    <ul class="sidebar-nav">
      <?php ($this->uri->segment(1) == "Dashboard" && empty($this->uri->segment(2))) ? $dashactive = "active" : $dashactive = ""; ?>
      <?php if(in_array('STATISTICS',$_SESSION['rows_exploded'])) : ?>
      <li class="<?= @$dashactive ?>">
        <a href="<?= base_url() ?>dashboard">
          <div class="icon">
            <i class="fa fa-tasks" aria-hidden="true"></i>
          </div>
          <div class="title">Dashboard</div>
        </a>
      </li>
      <?php endif; ?>

      <!-- <?php ($this->uri->segment(2) == "POS") ? $posactive = "active" : $posactive = ""; ?>
      <?php if(in_array('POS',$_SESSION['rows_exploded'])) : ?>
      <li class="@@menu.messaging <?= @$posactive ?>">
        <a href="<?= base_url() ?>dashboard/pos">
          <div class="icon">
            <i class="fa fa-money" aria-hidden="true"></i>
          </div>
          <div class="title">Sales</div>
        </a>
      </li>
      <?php endif; ?> -->

      <?php ($this->uri->segment(1) == "BackOffice") ? $backoffactive = "active" : $backoffactive = ""; ?>
      <?php if(in_array('CASH',$_SESSION['rows_exploded']) || in_array('PROD',$_SESSION['rows_exploded']) || in_array('SUPCUST',$_SESSION['rows_exploded']) || in_array('ORDER',$_SESSION['rows_exploded']) ) : ?>
      <li class="dropdown <?= $backoffactive; ?>">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <div class="icon">
            <i class="fa fa-cube" aria-hidden="true"></i>
          </div>
          <div class="title">BackOffice</div>
        </a>
        <div class="dropdown-menu">
          <ul>
            <li class="section"><i class="fa fa-cube" aria-hidden="true"></i> Sales History</li>
            
            <?php if(in_array('CASH',$_SESSION['rows_exploded'])) : ?>
            <li><a href="<?= base_url()?>office/cashmanage">Cash Management</a></li>
            <?php endif; ?>

            <?php if(in_array('PROD',$_SESSION['rows_exploded'])) : ?>
            <li><a href="<?= base_url()?>office/manage_product">Manage Product</a></li>
            <?php endif; ?>

            <?php /* if(in_array('SUPCUST',$_SESSION['rows_exploded'])) : ?>
            <li><a href="<?= base_url()?>backoffice/vendor">Vendors</a></li>
            <?php endif; ?>

            <?php if(in_array('ORDER',$_SESSION['rows_exploded'])) : ?>
            <li><a href="<?= base_url()?>backoffice/order">Order</a></li>
            <?php endif; ?>

            <?php if(in_array('SUPCUST',$_SESSION['rows_exploded'])) : ?>
            <li><a href="<?= base_url()?>backoffice/customers">Customers</a></li>
            <?php endif; */ ?>

            <li class="line"></li>
            <li class="section"><i class="fa fa-cube" aria-hidden="true"></i> Product Info Registration</li>

            <?php if(in_array('PROD',$_SESSION['rows_exploded'])) : ?>
            <li><a href="<?= base_url()?>office/product_register">Register Product</a></li>
            <?php endif; ?>

          </ul>
        </div>
      </li>
      <?php endif; ?>

      <?php ($this->uri->segment(1) == "Report") ? $reportactive = "active" : $reportactive = ""; ?>
      <?php if(in_array('INV',$_SESSION['rows_exploded']) || in_array('GNL',$_SESSION['rows_exploded']) ) : ?>
      <li class="dropdown <?= $reportactive ?>">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <div class="icon">
            <i class="fa fa-file-o" aria-hidden="true"></i>
          </div>
          <div class="title">Report</div>
        </a>
        <div class="dropdown-menu">
          <ul>
            <li class="section"><i class="fa fa-file-o" aria-hidden="true"></i> Report</li>

            <?php /* if(in_array('CASH',$_SESSION['rows_exploded'])) : ?>
            <li><a href="<?= base_url()?>report/gl">Cash Report</a></li>
            <?php endif; ?>

            <?php if(in_array('INV',$_SESSION['rows_exploded'])) : ?>
            <li><a href="<?= base_url()?>report/inventoryreport">Inventory Report</a></li>
            <?php endif; */ ?>

            <?php if(in_array('GNL',$_SESSION['rows_exploded'])) : ?>
            <li><a href="<?= base_url()?>report/general_report">General Report</a></li>
            <?php endif; ?>
            
            <li class="line"></li>
          </ul>
        </div>
      </li>
      <?php endif; ?>

      <?php ($this->uri->segment(1) == "Administration") ? $adminactive = "active" : $adminactive = ""; ?>
      <?php if(in_array('USERS',$_SESSION['rows_exploded']) || in_array('SETTINGS',$_SESSION['rows_exploded'])  || in_array('ROLES',$_SESSION['rows_exploded']) ) : ?>
      <li class="dropdown <?= $adminactive ?>">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <div class="icon">
            <i class="fa fa-gears" aria-hidden="true"></i>
          </div>
          <div class="title">Admin</div>
        </a>
        <div class="dropdown-menu">
          <ul>
            <li class="section"><i class="fa fa-gears" aria-hidden="true"></i> Administration</li>
            
            <?php if(in_array('USERS',$_SESSION['rows_exploded'])) : ?>
            <li><a href="<?= base_url() ?>administration/users">Users</a></li>
            <?php endif; ?>

            <?php if(in_array('SETTINGS',$_SESSION['rows_exploded'])) : ?>
            <li><a href="<?= base_url() ?>administration/settings">Setting</a></li>
            <?php endif; ?>

            <?php if(in_array('ROLES',$_SESSION['rows_exploded'])) : ?>
            <li><a href="<?= base_url() ?>administration/priviledges">Priviledgs</a></li>
            <?php endif; ?>

          </ul>
        </div>
      </li>
      <?php endif; ?>
      
    </ul>
  </div>
  <div class="sidebar-footer">
    <ul class="menu">
      <li>
        <a href="/" class="dropdown-toggle" data-toggle="dropdown">
          <i class="fa fa-cogs" aria-hidden="true"></i>
        </a>
      </li>
      <li><a href="#">marksBon &trade;</a></li>
    </ul>
  </div>
</aside>