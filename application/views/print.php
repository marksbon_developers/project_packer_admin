  <head>
  <title>Invoice</title>
  
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/vendor.css">
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/flat-admin.css">

  <!-- Theme -->
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/theme/blue-sky.css">
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/theme/blue.css">
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/theme/red.css">
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/theme/yellow.css">

</head>



<body onload="possubmit()">
    <div class="wrapper">
      <!-- Main content -->
      <section class="invoice">
        <div class="row">
          <div class="col-xs-12">
          <div class="col-xs-4 invoice-col">
           <!-- <h2 class="page-header"> -->
            <img src="<?= base_url().@$_SESSION['company_logo'] ?>" style="margin-left:0em;width:150px;height:60px;" class="">
            </h2>
            <!--</div>--><hr></hr>
          </div>
            <div class="col-xs-4 invoice-col" style="margin-top:60px;"><hr></hr>  </div>
              <div class="col-xs-4 invoice-col">
                
            <address style="height:60px;">
              <strong><?= @$_SESSION['company_name'] ?></strong><br>
              <?= @$_SESSION['company_address'] ?><br>
              <?= @$_SESSION['company_location'] ?><br>
              <?= @$_SESSION['company_tel'] ?> / 
              <?= @$_SESSION['company_alttel'] ?><br>
           
      
            </address>
            <hr></hr>
          <!--<h2 class="page-header"></h2>-->
          </div><!-- /.col -->
          
          </div><!-- /.col -->
        </div>
        <!-- info row -->
       
       <?= $formsubmit ?>

        <!-- Table row -->
        <div class="row">
          <div class="col-xs-12">
            <table class="table-striped primary" cellspacing="0" width="100%">
              <thead>
                <tr>
                   <th>#</th>
                   <th>Product</th>
                  <th>Qty</th>
                  <th>Unit Price</th>
                  <th>Subtotal</th>
                </tr>
              </thead>
              <tbody>
                <?php
                  if(!empty($printpreview)) :
                    $counter = 1;
                    for($a = 0; $a < sizeof($printpreview['prodname']); $a++) :
                ?>
                <tr>
                  <td><?= $counter ?></td>
                  <td><?= $printpreview['prodname'][$a] ?></td>
                  <td><?= $printpreview['prodqty'][$a] ?></td>
                  <td><?= $printpreview['produnit'][$a] ?></td>
                  <td><?= $printpreview['prodqty'][$a] * $printpreview['produnit'][$a] ?></td>
                </tr>
                <?php
                      $counter++;
                    endfor;
                  endif;
                ?>
              </tbody>
            </table>
          </div><!-- /.col -->
        </div><!-- /.row -->
        <hr></hr>
        <div class="row">
          <!-- accepted payments column -->
          <div class="col-xs-7">
            <div class="table-responsive">
             <div class="col-xs-8 invoice-col">
            <b>Invoice #007612</b><br>
             <?= @$_SESSION['company_email'] ?><br>
              <?= @$_SESSION['company_website'] ?><br>
            Date:<b ><?= gmdate("d F, Y")?></b>
          </div><!-- /.col -->
            </div>
          </div><!-- /.col -->
          <div class="col-xs-5">
            <div class="pull-right">
              <table class="">
                <tr>
                  <th style="width:50%">Subtotal:</th>
                  <td>GHȻ <?= $printpreview['prodtotcost'] ?></td>
                </tr>
                <tr>
                  <th>Tax (17.5%)</th>
                  <td>GHȻ <?= round($printpreview['prodtotcost'] * 0.175,2) ?></td>
                </tr>
                <tr>
                  <th>Total:</th>
                  <td>GHȻ <?= $printpreview['prodtotcost'] ?></td>
                </tr>
              </table>
            </div>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </section><!-- /.content -->
    </div><!-- ./wrapper -->

    <!-- AdminLTE App -->
   
  </body>