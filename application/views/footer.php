    <footer class="app-footer"> 
      <div class="row">
        <div class="col-xs-12">
          <div class="footer-copyright">
            <hr></hr>
           <center> Powered by: marksBon Co,Ltd. &reg Copyright © 2017  </center>
          </div>
        </div>
      </div>
    </footer>
    <?php require_once "modals.php"; ?>
  </div>
</div>

  <script type="text/javascript" src="<?= base_url() ?>resources/js/vendor.js"></script> 
  <script type="text/javascript" src="<?= base_url() ?>resources/js/app.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>resources/js/datepicker/moment.min.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>resources/js/datepicker/daterangepicker.js"></script>
  <!-- Retain Tab Active -->
  
  <!-- Retain Tab Active -->
  <!-- General Functions -->
    <script type="text/javascript">
      /************ Document Ready *********************/
      $(document).ready(function(){
        /************ Modal Functions *********************/
          var resulturl = location.href;
          $('#resulturl').val(location.href) ;
          $('[name="resulturl"]').val(resulturl);
          /****** Normal Delete Btn ******/
          $(".expenses").click(function(){ 
            $('#expences').modal('show');
          });
          /****** Normal Delete Btn ******/
          /****** Normal Delete Btn ******/
          $(".deletebtn").click(function(){ // Click to only happen on announce links
            $("#form_url").attr('action', $(this).data('formurl'));
            $("#deleteId").val($(this).data('delid'));
            $("#deletename").text($(this).data('delname'));
            $('[name="resulturl"]').val(resulturl);
            $('#deletemodal').modal('show');
          });
          /****** Normal Delete Btn ******/
          /****** Table Delete Btn  ******/
          $(".table").on("click", ".deletebtn", function(){
            $("#form_url").attr('action', $(this).data('formurl'));
            $("#deleteId").val($(this).data('delid'));
            $("#deletename").text($(this).data('delname'));
            $('#deletemodal').modal('show');
          });
          /****** Table Delete Btn  ******/
          /****** Table Edit Btn  ******/
          // Category Edit
          $(".table").on("click", ".editcat", function(){
            $("#editid").val($(this).data('editid'));
            $("#oldname").val($(this).data('editname'));
            $('#catdescmodal').modal('show');
          });

           /****** Table Edit Btn  ******/
          // Category Edit
          $(".table").on("click", ".viewcash", function(){
            var id = $(this).data('id');

            $.ajax({
              type: 'POST',
              url: '<?= base_url() ?>backoffice/return_cashreport',
              data: {cashid: id},
              success: function(response){
                if(response)
                {
                  document.getElementById("cashdet").innerHTML = response;
                  //alert(response);
                }
                else 
                {
                  document.getElementById("cashdet").innerHTML = "<tr><td>Retrieve Failed</td></tr>";
                }
              }   
            });
            $('#salesledger').modal('show');
          });
          
          // Employee Full Details
          $(".table").on("click", ".userdetails", function(){
            //$("#cateditform").attr('action', $(this).data('url'));
            $('[name="last_id"]').val($(this).data('key'));
            $('[name="edit_employee_id"]').val($(this).data('empid'));
            $('[name="edit_fullname"]').val($(this).data('name'));
            $('[name="edit_dob"]').val($(this).data('dob'));
            $('.empgender option[value="'+ $(this).data('gender') + '"]').attr('selected', 'selected');
            $('[name="edit_emergencyname"]').val($(this).data('emename'));
            $('[name="edit_emergencytel"]').val($(this).data('emetel'));
            $('[name="edit_resAddr"]').val($(this).data('res'));
            $('[name="edit_tel1"]').val($(this).data('tel1'));
            $('[name="edit_tel2"]').val($(this).data('tel2'));
            $('[name="edit_email"]').val($(this).data('email'));
            $('#employee_modal').modal('show');
          });

          // User Roles Edit
          $(".table").on("click", ".user_details", function(){
            userid = $(this).data('userk');
            $.ajax({
              type: 'POST',
              url: '<?= base_url() ?>access/roles_retrieve',
              data: {userid: userid},
              success: function(roles_response){
                if(roles_response)
                {
                  document.getElementById("rolesview").innerHTML=roles_response;
                }
                else 
                {
                  document.getElementById("rolesview").innerHTML="Roles Retrieve Failed";
                }
              }   
            });
            $('#usereditmodal').modal('show');
          });

            
          /****** Table Edit Btn  ******/
          /****** User Reset Btn    ******/
          $(".resetbtn").click(function(){ 
            $("#resetname").val($(this).data('username'));
            $("#resetId").val($(this).data('userde'));
            $('#resetmodal').modal('show');
          });
          /****** User Reset Btn    ******/
          /****** Employee Edit Btn    ******/
          $(".editempbtn").click(function(){ 
            $('[name="edit_employee_id"]').removeAttr('readonly');
            $('[name="edit_fullname"]').removeAttr('readonly');
            $('[name="edit_dob"]').removeAttr('readonly');
            $('.empgender').removeAttr('disabled');
            $('[name="edit_emergencyname"]').removeAttr('readonly');
            $('[name="edit_emergencytel"]').removeAttr('readonly');
            $('[name="edit_resAddr"]').removeAttr('readonly');
            $('[name="edit_tel1"]').removeAttr('readonly');
            $('[name="edit_tel2"]').removeAttr('readonly');
            $('[name="edit_email"]').removeAttr('readonly');
            $('.editempbtn').hide();
            $('.empupdatebtn').show();
            $('#employee_modal').modal('show');
          });
          $('#employee_modal').on('hidden.bs.modal',function(e){
            $('[name="edit_employee_id"]').attr('readonly','readonly');
            $('[name="edit_dob"]').attr('readonly','readonly');
            $('.empgender').attr('disabled','disabled');
            $('[name="edit_emergencyname"]').attr('readonly','readonly');
            $('[name="edit_emergencytel"]').attr('readonly','readonly');
            $('[name="edit_resAddr"]').attr('readonly','readonly');
            $('[name="edit_tel1"]').attr('readonly','readonly');
            $('[name="edit_tel2"]').attr('readonly','readonly');
            $('[name="edit_email"]').attr('readonly','readonly');
            $('.editempbtn').show();
          });
          
          /****** Employee Edit Btn    ******/
          /****** Product Details Btn  ***/
          $(".prod-details-mod").click(function(){ 
            $("#prodid").text($(this).data('productid'));
            $("#prodcategory").text($(this).data('prodcategory'));
            $("#prodname").text($(this).data('prodname'));
            $("#proddesc").text($(this).data('proddesc'));
            $("#prodloc").text($(this).data('prodloc'));
            $("#produnitqty").text($(this).data('produnitqty'));
            $("#produnitprice").text($(this).data('produnitprice'));
            $("#prodcurrqty").text($(this).data('prodcurrqty'));
            $("#prodexpiry").text($(this).data('prodexpiry'));
            $('#managestock-details').modal('show');
          });
          /****** Product Details Btn  ***/
          /****** Edit Supplier Btn  *****/
          $("#allsuppliers").on("click", ".supdetails", function(){
            $('[name="edit_sup_id"]').val($(this).data('supid'));
            $('[name="edit_sup_name"]').val($(this).data('name'));
            $('[name="edit_sup_tel1"]').val($(this).data('tel1'));
            $('[name="edit_sup_tel2"]').val($(this).data('tel2'));
            $('[name="edit_sup_addr"]').val($(this).data('addr'));
            $('[name="edit_sup_email"]').val($(this).data('email'));
            $('[name="edit_sup_loc"]').val($(this).data('loc')); 
            $('#paytype option[value="'+ $(this).data('paytype') + '"]').attr('selected', 'selected');
            if($(this).data('paytype') == "Cheque")
            {
              $('.bank').show();
              $('[name="bank"]').val($(this).data('bank')); 
              $('.branch').show();
              $('[name="branch"]').val($(this).data('branch')); 
              $('.accname').show();
              $('[name="acc_name"]').val($(this).data('acctname')); 
              $('.accno').show();
              $('[name="acc_no"]').val($(this).data('num')); 
              
              $.ajax({
              type: 'POST',
              url: '<?= base_url() ?>backOffice/retrieve_prod_type',
              data: {sup_id: $(this).data('supid')},
              success: function(prod_type){
                if(prod_type)
                {
                  document.getElementById("producttype").innerHTML=prod_type;
                }
                else 
                {
                  document.getElementById("producttype").innerHTML="No Product Type Registered";
                }
              }   
            });
            }
            $('#edit-supplier').modal('show');
          });

          /*  Edit Suppliers Btn   */
          $(".editsupbtn").click(function(){ 
            $('[name="edit_sup_name"]').removeAttr('readonly');
            $('[name="edit_sup_tel1"]').removeAttr('readonly');
            $('[name="edit_sup_tel2"]').removeAttr('readonly');
            $('[name="edit_sup_addr"]').removeAttr('readonly');
            $('[name="edit_sup_email"]').removeAttr('readonly');
            $('[name="edit_sup_tel2"]').removeAttr('readonly');
            $('[name="edit_sup_loc"]').removeAttr('readonly');
            $('[name="bank"]').removeAttr('readonly');
            $('[name="branch"]').removeAttr('readonly');
            $('[name="acc_name"]').removeAttr('readonly');
            $('[name="acc_no"]').removeAttr('readonly');
            $('.editsupbtn').hide();
            $('.supupdatebtn').show();
            $('#edit-supplier').modal('show');
          });
          /*  Edit Suppliers Btn   */

          $('#edit-supplier').on('hidden.bs.modal',function(e){
            document.getElementById("paytype").innerHTML="<option></option><option value='Cheque'>Cheque</option><option value='Cash'>Cash</option>";
            $('.bank').hide();
            $('.branch').hide();
            $('.accno').hide();
            $('.accname').hide();
          });

          /*  Edit Product Btn   */
          $('.datatable').on('click',".editproduct",function(){ 
            $('[name="edit_prod_id"]').val($(this).data('productid'));
            $('[name="edit_prod_name"]').val($(this).data('prodname'));
            $('[name="edit_prod_code"]').val($(this).data('prodcode'));
            $('[name="edit_unit_qty"]').val($(this).data('produnitqty'));
            $('[name="edit_cur_qty"]').val($(this).data('prodcurqty'));
            $('[name="edit_unit_price"]').val($(this).data('produnitprice')); 
            $('[name="edit_unit_promoqty"]').val($(this).data('promoqty')); 
            $('[name="edit_unit_promoprice"]').val($(this).data('promoprice')); 
            $('#editdesc option[value="'+ $(this).data('descid') + '"]').attr('selected', 'selected');
            $('#updateprodmodal').modal('show');
          });

          $('#edit-supplier').on('hidden.bs.modal',function(e){
          });
          /*  Edit Suppliers Btn   */
          /****** Edit Supplier Btn  *****/
        /************ Modal Functions *********************/
      });
      /*********** Statistics Graphs ********************/
      <?php 
        if(!empty($allstores_data)) : $counter = 1;
          foreach($allstores_data as $shop=>$value) :
            $label = $values= "";  
            if(!empty($value['all_products_sold'])) : 
              foreach($value['all_products_sold'] as $product=>$qty) :
                $label = $label."'".$product."',";
                $values = $values."'".$qty."',";
              endforeach;
            endif;
      ?>
            /*var data = {
              series: [<?=$values?>]
            };

            var sum = function(a, b) { return a + b };

            new Chartist.Pie('.ct-chart-browser', data, {
              labelInterpolationFnc: function(value) {
                return Math.round(value / data.series.reduce(sum) * 100) + '%';
              }
            });*/

            var chartData = {
              labels: [<?=$label?>],
              datasets: [{
                label: "Population (millions)",
                backgroundColor: ["#29c75f", "#39c3da","#666","#c8d1d3","#8d9293"],
                data: [<?=$values?>]
              }]
            };

          drawPie('piechart<?=$counter++?>',chartData);
      <?php 
          endforeach;
        endif;
      ?>
      /*********** Statistics Graphs ********************/
      /************ Document Ready *********************/
      function drawPie(id,chartData){
        let elementId = document.getElementById(id); 
        new Chart(elementId, {
          type: 'pie',
          data: chartData,
          options: {
            title: {
              display: false,
              text: ''
            },
            legend: {
              display: true,
              position: 'bottom',
              fullWidth: true,
            },
            tooltips: {
              callbacks: {
                label: function(tooltipItem, data) {
                  //get the concerned dataset
                  var dataset = data.datasets[tooltipItem.datasetIndex];
                  //calculate the total of this data set
                  var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
                    return parseFloat(previousValue) + parseFloat(currentValue);
                  });
                  //get the current items value
                  var currentValue = dataset.data[tooltipItem.index];
                  //calculate the precentage based on the total and current item, also this does a rough rounding to give a whole number
                  var precentage = Math.floor(((currentValue/total) * 100)+0.5);

                  return precentage + "%";
                  //return currentValue;
                }
              }
            }
          }
        });
      }
      /********* Unit / Amount Calculation ***********/
      function paymentterms(p_type)
      {
        if(p_type == "Cheque")
        {
          $('.bank').show(500);
          $('.branch').show(500);
          $('.accname').show(500);
          $('.accno').show(500);
        }
        else
        {
          $('.bank').hide(200);
          $('[name="bank"]').val(""); 
          $('.branch').hide(200);
          $('[name="branch"]').val(""); 
          $('.accname').hide(200);
          $('[name="acc_name"]').val(""); 
          $('.accno').hide(200);
          $('[name="acc_no"]').val(""); 
        }
      }

      /******** Unit / Price = Total Calculation (Promo) **************

      $(".table").on("input", ".qty", function(){
        
        var promo_price = $(this).data('promo_unit_price');
        var promo_qty = $(this).data('promo_unit_qty');


        if(promo_price != 0 && promo_qty != 0 ){
          var tr = $(this).closest("tr");
          var current_qty = tr.find('.qty').val();
          var unitprice = $(this).data('unitprice');
          
          if(current_qty >= promo_qty)
            tr.find(".unit").val(promo_price);

          else if(current_qty < promo_qty)
            tr.find(".unit").val(unitprice);

          compute();
        }
        
        
      });
      
      /******** Unit / Price = Total Calculation **************/
      $(".qty").on('input',compute);
      //$(".unit").on('input',compute);

      function compute()
      {
        var tr = $(this).closest("tr");
        //Promo Check 
        var promo_price = $(this).data('promo_unit_price');
        var promo_qty = $(this).data('promo_unit_qty');

        if(promo_price != 0 && promo_qty != 0 ) {
          
          var qtyVal = tr.find('.qty').val();
          var unitVal = $(this).data('unitprice');

          if(qtyVal >= promo_qty)
          {
            tr.find(".unit").val(promo_price);
            var unitVal = tr.find(".unit").val();
          }

          else if(qtyVal < promo_qty)
          {
            tr.find(".unit").val(unitVal);
            var unitVal = tr.find(".unit").val();
          }
        }

        else {
          var qtyVal = tr.find('.qty').val();
          var unitVal = tr.find(".unit").val();
        }

        

        if(typeof qtyVal == "undefined" || typeof unitVal == "undefined")
          return;

        tr.find(".totalamt").val(qtyVal * unitVal);

        fnAlltotal();
      }

      function fnAlltotal()
      {
        var total = 0;
        $(".totalamt").each(function(){
          total += parseFloat($(this).val()||0)
        });

        $("#totalcost").val(total);
        $("#balance").val(total);


        $("#totcost").val(total);
        $("#checkout").text(total);
      }

      $('#pay').on('input', function(){
        var balance = $("#totalcost").val() - $("#pay").val();

        if(balance < 0)
        {
          $('#amtexceed').modal('show');
          $("#pay").val('0');
          $("#balance").val($("#totalcost").val());
        }
          
        
        else
          $("#balance").val(balance);
      });

      function addtotally(obj)
      {
        var prodname = obj.getAttribute('data-prodname');
        var unitprice = obj.getAttribute('data-unitprice');
        var promounitprice = obj.getAttribute('data-promounitprice');
        var promounitqty = obj.getAttribute('data-promounitqty');
        
        $('#tallytbl tr:last').after('<tr><td style="color:red"><i class="fa fa-close del_req_row" style="cursor:pointer"></i></td><td><input type="hidden" name="prodname[]" value="'+prodname+'"/>'+prodname+'</td><td><input type="number" value="0" min="1" class="me qty" name="qty[]" data-unitprice="'+unitprice+'"  data-promo_unit_price="'+promounitprice+'" data-promo_unit_qty="'+promounitqty+'" /></td><td><input type="number" placeholder="1" min="1" class="me unit promocheck" name="unit[]" value="'+unitprice+'" readonly/></td><td><input type="number" placeholder="0" min="1" class="me totalamt" name="totalsum[]" readonly /></td></tr>');

        $(".qty").on('input',compute);
        $(".unit").on('input',compute);

      }

      $(".table").on("click", ".del_req_row", function(){
        $(this).closest('tr').remove();
        fnAlltotal();
      });

      $('.checkout').click(function(){ 
        $('#checkoutform').submit();
      });

      function possubmit()
      {
          window.print();
          $('#possave').submit();
      }

      /********* Tools / MAterial ***********/

      /********** Add More To Stock Update ********/
      $(".table").on("click", ".addmore", function(){

        var products = "<?php if(!empty($productnames)) { foreach ($productnames As $prod) { print "<option value='".base64_encode($prod->prod_id)."'>".$prod->prod_name."</option>"; } } ?>";

        var category = "<?php if(!empty($Description_info)) { foreach ($Description_info As $desc) { print "<option value='".base64_encode($desc->desc_id)."'>".$desc->desc."</option>"; } } ?>";


        $('#newstock tr:last').after('<tr><td><select class="form-control" name="prod_id[]"><option></option>'+products+'</select></td><td><select class="form-control" name="cat_id[]"><option></option>'+category+'</select></td><td><input type="number" class="form-control" name="costP[]" placeholder="100" required style="height:33px !important;"/></td><td><input type="number" min="1" class="form-control qty" name="qty[]" placeholder="100" required style="height:33px !important;"/></td><td><input id="unit" type="number" class="form-control unit" name="unit[]" placeholder="20" required style="height:33px !important;"/></td><td><input id="totalprice" type="number" class="form-control totalamt" name="sup[]" placeholder="20" required style="height:33px !important;" disabled/></td><td><a href="" title="Add New" class="btn btn-primary btn-xs addmore"><i class="fa fa-plus"></i></a> <a href="" title="Delete" class="btn btn-danger btn-xs del_req_row"><i class="fa fa-trash"></i></a></td></tr>');

        $(".qty").on('input',compute);
        $(".unit").on('input',compute);
        
      });
      /********** Add More To Stock Update ********/

    </script>
    <!-- General Functions -->
    <script type="text/javascript" src="<?php print base_url(); ?>resources/js/notify/pnotify.core.js"></script>
    <script type="text/javascript" src="<?php print base_url(); ?>resources/js/notify/pnotify.buttons.js"></script>
    <script type="text/javascript" src="<?php print base_url(); ?>resources/js/notify/pnotify.nonblock.js"></script>
    <!--*********** Notification    *********************-->
    <script type="text/javascript">
      <?php if(!empty($_SESSION['success'])) : ?>
        /**** Success Notification ****/
        var permanotice, tooltip, _alert;
        $(function () {
          new PNotify({
              title: 'Process Successful',
              text: '<?= $this->session->flashdata("success") ?>',
              type: 'success'
          });
        });
        /**** Success Notification ****/
      <?php elseif (!empty($_SESSION['error'])) : ?>
        /**** Error Notification ****/
        var permanotice, tooltip, _alert;
        $(function () {
          new PNotify({
              title: 'An Error Occurred',
              <?php $this->session->set_flashdata("error",@$_SESSION[error]); ?>
              text: '<?= $this->session->flashdata("error") ?>',
              type: 'error'
          });
        });
        /**** Error Notification ****/
        <?php elseif (validation_errors()) : ?>
        var permanotice, tooltip, _alert;
        $(function () {
          new PNotify({
              title: 'An Error Occurred',
              text: '<?= validation_errors() ?>',
              type: 'error'
          });
        });
        /**** Error Notification ****/
        /**** Warning Notification ****/
        <?php elseif (!empty($_SESSION['warning'])) : ?>
        var permanotice, tooltip, _alert;
        $(function () {
          new PNotify({
              title: 'No Record(s) Found',
              text: '<?= $this->session->flashdata("warning") ?>',
              type: 'warning'
          });
        });
        /**** Warning Notification ****/
      <?php endif; ?>
    </script>
    <!-- Warning Notification -->
      
    <!--*********** Notification    *********************-->
    <?php unset($_SESSION['error']); ?>

    <!-- ********* Ajax Calls **************** -->
    <script type="text/javascript">
      
      function fetch_itms_from_category(descid)
      {
        $.ajax({
          type: 'POST',
          url: 'prod_ret_ajax_cat',
          data: {desc_id: descid},
          success: function(prod_response){
            if(prod_response)
            {
              document.getElementById("ProductDisplay").innerHTML=prod_response;
            }
            else 
            {
              $('#noproductmodal').modal('show');
            }
          }   
        });
      }

      function fetch_itms(product_id)
      {
        $.ajax({
          type: 'POST',
          url: 'prod_ret_ajax',
          data: {productid: product_id},
          success: function(prod_response){
            if(prod_response)
            {
              document.getElementById("ProductDisplay").innerHTML=prod_response;
            }
            else 
            {
              alert("Failed");
            }
          }   
        });
      }

      function fetch_desc(cat_id)
      {
        $.ajax({
          type: 'POST',
          url: 'desc_ret_ajax',
          data: {catid: cat_id},
          success: function(desc_response){
            if(desc_response)
            {
              document.getElementById("catdesc").innerHTML=desc_response;
            }
            else 
            {
                alert("Failed");
            }
          }   
        });
      }

      function fetch_acct_desc(acct_id)
      {
        var table = "account_types";

        $.ajax({
          type: 'POST',
          url: '<?= base_url() ?>Dashboard/return_row_data',
          data: {id: acct_id,tablename:table},
          success: function(response){
            if(response)
            {
              var acc_into = response.split('/');
              $(".acct_desc").val(acc_into[0]);
              $(".acct_code").val(acc_into[1]);
            }
            
          }   
        });
      }

      function fetch_roles(userid)
      {
        if(userid)
        {
          $.ajax({
            type: 'POST',
            url: '<?= base_url() ?>access/setroles_retrieve',
            data: {userid: userid},
            success: function(roles_response){
              if(roles_response)
              {
                  roles_exploded = roles_response.split('|');
                  
                  if($.inArray('STATISTICS',roles_exploded) != '-1')
                    $('#role_statistics').prop('checked', true);
                  else
                    $('#role_statistics').prop('checked', false);
                  
                  if($.inArray('POS',roles_exploded) != '-1')
                    $('#role_pos').prop('checked', true);
                  else
                    $('#role_pos').prop('checked', false);
                  
                  if($.inArray('SUPCUST',roles_exploded) != '-1')
                    $('#role_sup_cust').prop('checked', true);
                  else
                    $('#role_sup_cust').prop('checked', false);
                  
                  if($.inArray('PROD',roles_exploded) != '-1')
                    $('#role_prod').prop('checked', true);
                  else
                    $('#role_prod').prop('checked', false);
                  
                  if($.inArray('CASH',roles_exploded) != '-1')
                    $('#role_cash').prop('checked', true);
                  else
                    $('#role_cash').prop('checked', false);
                  
                  if($.inArray('USERS',roles_exploded) != '-1')
                    $('#role_user').prop('checked', true);
                  else
                    $('#role_user').prop('checked', false);
                  
                  if($.inArray('ROLES',roles_exploded) != '-1')
                    $('#role_priv').prop('checked', true);
                  else
                    $('#role_priv').prop('checked', false);
                  
                  if($.inArray('REPORT',roles_exploded) != '-1')
                    $('#role_rep').prop('checked', true);
                  else
                    $('#role_rep').prop('checked', false);

                  if($.inArray('ORDER',roles_exploded) != '-1')
                    $('#role_order').prop('checked', true);
                  else
                    $('#role_order').prop('checked', false);
                  
                  if($.inArray('SETTINGS',roles_exploded) != '-1')
                    $('#role_set').prop('checked', true);
                  else
                    $('#role_set').prop('checked', false);

                  if($.inArray('INV',roles_exploded) != '-1')
                    $('#role_iledger').prop('checked', true);
                  else
                    $('#role_iledger').prop('checked', false);

                  if($.inArray('GNL',roles_exploded) != '-1')
                    $('#role_gledger').prop('checked', true);
                  else
                    $('#role_gledger').prop('checked', false);
              }
              else 
              {
                $('#norolesmodal').modal('show');
              }
            }   
          });
        }
        else
        {
          $('#role_statistics').prop('checked', false);
          $('#role_pos').prop('checked', false);
          $('#role_sup_cust').prop('checked', false);
          $('#role_prod').prop('checked', false);
          $('#role_cash').prop('checked', false);
          $('#role_user').prop('checked', false);
          $('#role_priv').prop('checked', false);
          $('#role_rep').prop('checked', false);
          $('#role_set').prop('checked', false);
        }
        
      }

      /******** Click Event For Daily Sales Report *********/
      function viewOrderDetails(element) {
          let transid = $(element).data('trans');
          let resource = $(element).data('resource');
          $.ajax({
              type: 'POST',
              url: '<?= base_url() ?>dashboard/return_daily_salesreport',
              data: {trans_id: transid, resource: resource},
              success: function(response){
                  if(response)
                  {
                      $('#salesrep_details').html(response);
                      $('#salesrep_modal').modal('show');
                  }
                  else
                      alert("Failed");
              }
          });
      }
      /******** Click Event For Daily Sales Report *********/

      /******** Manage Product Table *********
        $('#manage_product_tbl').datatable({
          data: "",
          "columns"    : [
            {'data': "Item_Name"},
          ]
        });
      /******** Manage Product Table *********/

      /******** Date Picker *********/
        $('.datepicker').daterangepicker({
            "showDropdowns": true,
            "autoApply": true,
            ranges: {
               'Today': [moment(), moment()],
               'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
               // i want last 7 days as default
               'Last 7 Days': [moment().subtract(6, 'days'), moment()],
               'Last 30 Days': [moment().subtract(29, 'days'), moment()],
               'This Month': [moment().startOf('month'), moment().endOf('month')],
               'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            locale: {
              format: "DD-MM-YYYY",
              separator: "  To  "
            },
            "startDate": "01-01-2018",
            "endDate": "31-01-2018"
        }, function(start, end, label) {
          console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
        });
      /******** Date Picker *********/

      /******** Bar Chart *********/
      <?php if(@$monthly_stat) : ?>
      var data = {
        labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        series: [
          <?php 
            foreach($monthly_stat as $value) : 
              $array = json_decode(json_encode($value), true);
              $month_values = implode(',', $array);
              print "[".$month_values."],";
            endforeach;
          ?>
        ]
      };

      var options = {
        seriesBarDistance: 14,
        height: 350,
      };

      var responsiveOptions = [
        ['screen and (max-width: 600px)', {
          seriesBarDistance: 14,
          axisX: {
               // On the x-axis start means top and end means bottom
               position: 'start'
           },
           axisY: {
               // On the y-axis start means left and end means right
               position: 'end',
               onlyInteger: true,
           }
        }]
      ];

      new Chartist.Bar('.ct-chart-os', data, options, responsiveOptions);
      <?php endif; ?>
      /******** Bar Chart *********/

      /* Datatable Configurations */
      var previousMonth;
      $('#monthly_stats').dataTable({
        searching: false,
        paging: false,
        autoWidth: false,
        order: [],
        ajax: {
          type : 'GET',
          dataType : 'json',
          url : '<?= base_url()?>dashboard/getmonthlystat',
          dataSrc: '',
          error: function(response){}
        },
        columns: [
          {render: function(data,type,row,meta){
            let currentMonth = row.monthname;
            if(previousMonth == currentMonth)
              currentMonth = "";
            else
            previousMonth = currentMonth;

            return '<b><span class="text-muted">'+currentMonth+'</span></b></a>';
          }},
          {data: 'storename'},
          {data: 'revenue'},
          {data: 'highest'},
        ]
      });
    </script>
  </body>
</html>