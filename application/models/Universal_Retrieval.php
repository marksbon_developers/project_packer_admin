<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Universal_Retrieval extends CI_Model 
{
  /*******************************
		Constructor 
	*******************************/
	public function index() 
  {
			#Redirect to Dashboard Where Roles Are Defined
			redirect('Access');
	}
    
  ############################### Generating ID ################################
    /***********************************************
			Generating New ID
    ************************************************/
    public function LastID($TableName,$FieldName) 
    {
      $this->db->select($FieldName);

      $query = $this->db->get($TableName);
        
      return ( ($query->num_rows() > 0) ? $query->row(0) : FALSE );
	  }
  ############################### Generating ID ################################
    
  ############################### All Data Info ################################
  	public function All_Info_Row($tablename) 
      {
          $query = $this->db->get($tablename);
              
          return ( ($query->num_rows() > 0) ? $query->row() :false );
  	}

    public function All_Info($tablename) 
    {
      $query = $this->db->get($tablename);
              
      return ( ($query->num_rows() > 0) ? $query->result() :false );
    }

    public function All_Info_API($tablename) 
    {
      $query = $this->db->get($tablename);
              
      return ( ($query->num_rows() > 0) ? json_encode($query->result()) :false );
    }
  ############################### All Data Info ################################
    
    ############################### Retrieval Table Fields ########################
    /***********************************************
            Retreieve Table Fields 
    ************************************************/
    
    public function ret_table_fields($tablename) 
    { 
        $result = $this->db->list_fields($tablename); 
        
        return(($result) ? $result : false);  
    }
    
    ############################### Retrieval With Single Condition ########################
    /***********************************************
            Retreieve Table Fields 
    ************************************************/
    public function ret_data_with_s_cond($tablename,$IdField,$data, $resource = null)
    {
       $tablename = $tablename;

       if($resource == null) {
          $this->db->where($IdField,$data[$IdField]);
          $query = $this->db->get_where($tablename);
       }
       else {
          //return STORES;
          $dbres = $this->load->database(STORES[$resource],TRUE);
          $dbres->where($IdField,$data[$IdField]);
          $query = $dbres->get_where($tablename);
       }
        

        
      return ( ($query->num_rows() > 0) ? $query->result() :false );
    }

    public function ret_data_with_s_cond_row($tablename,$IdField,$data) 
    { 
      $tablename = $tablename;
        
      $this->db->where($IdField,$data[$IdField]);
        //print $this->db->get_compiled_select($tablename);
      $query = $this->db->get_where($tablename); 
        
      return ( ($query->num_rows() > 0) ? $query->row() :false );
    }

    /***********************************************
            Retreieve Table Fields 
    ************************************************/
    public function ret_data_with_s_cond_boolean($tablename,$IdField,$data) 
    { 
      $tablename = $tablename;
        
      $this->db->where($IdField,$data[$IdField]);
        
      $query = $this->db->get_where($tablename); 
        
      return ( ($query->num_rows() > 0) ? TRUE : FALSE );
    }

    /***********************************************
            Retreieve Table Data 
    ************************************************/
    public function ret_data_with_s_cond_where($tablename,$whereCondition) 
    { 
      $tablename = $tablename;

      $this->db->where($whereCondition);
      //print $this->db->get_compiled_select($tablename);
      $query = $this->db->get($tablename); 
        
      return ( ($query->num_rows() > 0) ? $query->result() : FALSE );
    }

    /***********************************************
            Retreieve Table Data 
    ************************************************/
    public function ret_data_with_s_cond_where_like($tablename,$whereCondition,$likecondition) 
    { 
      $tablename = $tablename;

      $this->db->where($whereCondition);
        
      $this->db->like($likecondition);
//print $this->db->get_compiled_select($tablename);
      $query = $this->db->get($tablename); 
        
      return ( ($query->num_rows() > 0) ? $query->result() : FALSE );
    }

    /***********************************************
    		Retrive Various Stores Report
	 ***********************************************/
  public function all_stores_stat(){
    foreach (STORES as $key => $value) {
      $all_product_totals[$key] = array();
      $dbres = $this->load->database($value,TRUE);
      $tablename = "vw_product_transact";
      # ******* Retrieving Last Record *******
      $where_condition = $dbres->select_max('id');
      $date_retrieved = $dbres->get($tablename);
		  
      # ******* retrieving data with retrieved date *******
      $id = $date_retrieved->row();
      $where_condition = array('id' => $id->id);
      $query_result = $dbres->get_where($tablename,$where_condition);

      if($date = $query_result->row()) {
        $where_condition = array('DATE(date_withdrawn)' => gmdate('Y-m-d',strtotime($date->date_withdrawn)),'status' => "active");
        $query_result = $dbres->get_where($tablename,$where_condition);
        //print_r($query_result->result()); print "<br/><br/>"; continue;
        if($temp_data[$key] = $query_result->result()) {
          /********** Calculating Totals **********/
          foreach ($temp_data[$key] as $total_cost) {
            //print_r($total_cost); print "<br/><br/>"; continue;
            @$return_data[$key]['date'] = gmdate('l, d F Y',strtotime($date->date_withdrawn));
            @$return_data[$key]['total_revenue'] += $total_cost->tot_cost;
            @$return_data[$key]['total_tax'] += $total_cost->tax_value ;
            /******* Finding Top 5 Items Sold ************/
            $all_prod_id = explode('|', $total_cost->prod_id);
            $all_qty = explode('|', $total_cost->qty_withdrawn);

            for($a=0; $a < sizeof($all_qty); $a++) {
              /******** Retrieving Product Name ********/
              $tablename = "full_product_details";
              $dbres->select('Item_Name,Description');
              $where_condition = array('Product_ID' => @$all_prod_id[$a]);
              $query = $dbres->get_where($tablename,$where_condition);
              $productName_query_result = $query->row();
              if($productName_query_result) {
                $prod_name = @$productName_query_result->Item_Name." - ".@$productName_query_result->Description;
                /******** Retrieving Product Name ********/
                if(array_key_exists($prod_name, $all_product_totals[$key])){
                  @$all_product_totals[$key][$prod_name] += $all_qty[$a];
                }
                else
                  $all_product_totals[$key][$prod_name] = $all_qty[$a];
                  /*********** Variable Assignment *********/
                @$return_data[$key]['total_items_sold'] += $all_qty[$a];
              }
            }
            arsort($all_product_totals[$key],SORT_DESC);
            $return_data[$key]['all_products_sold'] = array_slice($all_product_totals[$key], 0,5, true);
            //print_r($return_data); print "<br/><br/>"; continue;
            /******* Finding Top 5 Items Sold ************/
          }
          /********** Calculating Totals **********/
        }
      }
    }
    return @$return_data;
  }

  /***********************************************
    Retrive Various Stores Report
  ***********************************************/
  public function admin_report($dbname,$tablename,$whereCondition,$store) {
    if($dbname) {
      $counter = 0;
      foreach ($dbname as $database_name) {
        $dbres = $this->load->database($database_name,TRUE);
        $dbres->where($whereCondition);
        $query_result = $dbres->get($tablename);
        $result_array = $query_result->result();

        foreach ($result_array as $key => $value) {
          # code...
          $value->store = @$store[$counter];
        }

        $result[] = $result_array; 

        $counter++;
      }
    }

    return $result;
    //print"<pre>";print_r($result_array);print"</pre>"; exit;
  }

  /***********************************************
    Retrive Various Stores Report
  ***********************************************/
  public function monthly_reports_stat(){
    $month_counter = 0;
    /*$all_stores = array(
      'Accra Mall'=>"packer_accramall",
      'West Hills Mall'=>"packer_westhills",
      'Junction Mall'=>"packer_junctionmall",
      'Achimota Retail Center'=>"packer_achimotamall"
    );*/
    $current_month = (int)date('m'); $current_year = (int) gmdate('Y');

    for ($monthNum=1; $monthNum <= $current_month ; $monthNum++) { 
      # code...
      $dateObj   = DateTime::createFromFormat('!m', $monthNum);
      $monthName = $dateObj->format('F'); // March
      
      foreach (STORES as $key => $value) {
        /**** Retrieving total revenue ******/
          $dbres = $this->load->database(STORES[$key],TRUE);
          $dbres->select_sum('tot_cost');
          $tablename = "vw_product_transact";
          $where_condition = $dbres->where(array('EXTRACT(YEAR_MONTH FROM date_withdrawn) =' => date('Ym',strtotime("$current_year-$monthNum")),'status' => "active"));
          $query_result = $dbres->get($tablename);
          //print $dbres->get_compiled_select($tablename);
          $total_cost = $query_result->row();
          $return_data[$monthName][$key]['revenue'] = $total_cost->tot_cost;
        /**** Retrieving total revenue ******/

        /**** Highest & Lowest product ****/
          $all_product_totals[$key] = array();
          $dbres = $this->load->database($value,TRUE);
          $tablename = "vw_product_transact";
          $where_condition = $dbres->where(array('EXTRACT(YEAR_MONTH FROM date_withdrawn) =' => date('Ym',strtotime("$current_year-$monthNum"))));
          $query_result = $dbres->get($tablename);
          //print "<pre>"; print_r($query_result->result());print "</pre>";  continue;
          if($query_result->result()) {
            $temp_data[$key] = $query_result->result();
            /********** Calculating Totals **********/
            foreach ($temp_data[$key] as $total_cost) {
              /******* Finding Top 5 Items Sold ************/
              $all_prod_id = explode('|', $total_cost->prod_id);
              $all_qty = explode('|', $total_cost->qty_withdrawn);

              for($a=0; $a < sizeof($all_qty); $a++) {
                /******** Retrieving Product Name ********/
                $tablename = "full_product_details";
                $this->db->select('Item_Name,Description');
                $where_condition = array('Product_ID' => @$all_prod_id[$a]);
                $query = $this->db->get_where($tablename,$where_condition); 
                $productName_query_result = $query->row();
                if($productName_query_result) {
                  $prod_name = @$productName_query_result->Item_Name." - ".@$productName_query_result->Description;
                  /******** Retrieving Product Name ********/
                  if(array_key_exists($prod_name, $all_product_totals[$key])){
                    @$all_product_totals[$key][$prod_name] += $all_qty[$a];
                  }
                  else
                    $all_product_totals[$key][$prod_name] = $all_qty[$a];
                    /*********** Variable Assignment *********/
                  @$return_data[$monthName][$key]['total_items_sold'] += $all_qty[$a];
                }
              }
              arsort($all_product_totals[$key],SORT_DESC);
              //print "<pre>";print_r($all_product_totals[$key]); print "</pre>";
              current($all_product_totals[$key]);
              $return_data[$monthName][$key]['highest'] = key($all_product_totals[$key]); 
              end($all_product_totals[$key]);
              $return_data[$monthName][$key]['least'] = key($all_product_totals[$key]); 
              /******* Finding Top 5 Items Sold ************/
            }
            /********** Calculating Totals **********/
          }
          else {
            $return_data[$monthName][$key]['total_items_sold'] = $return_data[$monthName][$key]['highest'] = $return_data[$monthName][$key]['least'] = 0;
          }
        /**** Highest & Lowest product ****/
      }
      /**** Retrieving highest & lowest product ****/
      
      $month_counter++;
    }
    //print "<pre>"; print_r($return_data);print "</pre>";  
    return $return_data;
  }

  public function monthly_reports_stat_new(){
    $month_counter = 0;
    /*$all_stores = array(
      'Accra Mall'=>"packer_accramall",
      'West Hills Mall'=>"packer_westhills",
      'Junction Mall'=>"packer_junctionmall",
      'Achimota Retail Center'=>"packer_achimotamall"
    );*/
    $current_month = (int)date('m'); 
    $current_year = (int) gmdate('Y');

    for ($monthNum=1; $monthNum <= $current_month ; $monthNum++) { 
      # code...
      $dateObj   = DateTime::createFromFormat('!m', $monthNum);
      $monthName = $dateObj->format('F'); // March
      
      foreach (STORES as $key => $value) {
        /**** Retrieving total revenue ******/
          $dbres = $this->load->database($value,TRUE);
          $dbres->select_sum('tot_cost');
          $tablename = "vw_product_transact";
          $where_condition = $dbres->where(array('EXTRACT(YEAR_MONTH FROM date_withdrawn) =' => date('Ym',strtotime("$current_year-$monthNum")),'status' => "active"));
          $query_result = $dbres->get($tablename);
          //print $dbres->get_compiled_select($tablename);
          $total_cost = $query_result->row();
          $return_data[] = [
            'totalstores' => NO_OF_STORES,
            'monthname' => $monthName,
            'storename' => $key,
            'revenue' => "Ȼ".number_format($total_cost->tot_cost),
            'total_items_sold' => 0,
            'highest' => "",
            'least' => 0,
          ];
        /**** Retrieving total revenue ******/

        /**** Highest & Lowest product ****/
          $all_product_totals[$key] = array();
          $where_condition = $dbres->where(array('EXTRACT(YEAR_MONTH FROM date_withdrawn) =' => date('Ym',strtotime("$current_year-$monthNum"))));
          $query_result = $dbres->get($tablename);
          
          if($query_result->result()) 
          {
            $temp_data[$key] = $query_result->result();
            $tempVar = array();
            /********** Calculating Totals **********/
            foreach ($temp_data[$key] as $total_cost) {
              /******* Finding Top 5 Items Sold ************/
              $all_prod_id = explode('|', $total_cost->prod_id);
              $all_qty = explode('|', $total_cost->qty_withdrawn);

              for($a=0; $a < sizeof($all_qty); $a++) {
                if(array_key_exists(@$all_prod_id[$a], $tempVar))
                  @$tempVar[$all_prod_id[$a]] += $all_qty[$a];
                else
                  $tempVar[$all_prod_id[$a]] = 0;
              }
            }
            # getting highest
            arsort($tempVar,SORT_DESC);

            /******** Retrieving Product Name ********/
            $tablename = "full_product_details";
            $dbres->select('Item_Name');
            $where_condition = array('Product_ID' => key($tempVar));
            $query = $dbres->get_where($tablename,$where_condition);
            $productName_query_result = $query->row();

            $return_data[sizeof($return_data) - 1]['highest'] = @$productName_query_result->Item_Name;

        //         /******** Retrieving Product Name ********/
        //         $tablename = "full_product_details";
        //         $this->db->select('Item_Name,Description');
        //         $where_condition = array('Product_ID' => $all_prod_id[$a]);
        //         $query = $this->db->get_where($tablename,$where_condition); 
        //         $productName_query_result = $query->row();
        //         if($productName_query_result) {
        //           $prod_name = @$productName_query_result->Item_Name." - ".@$productName_query_result->Description;
        //           /******** Retrieving Product Name ********/
        //           if(array_key_exists($prod_name, $all_product_totals[$key])){
        //             @$all_product_totals[$key][$prod_name] += $all_qty[$a];
        //           }
        //           else
        //             $all_product_totals[$key][$prod_name] = $all_qty[$a];
        //             /*********** Variable Assignment *********/
        //           @$return_data[$monthName][$key]['total_items_sold'] += $all_qty[$a];
        //         }
        //      }
        //       arsort($all_product_totals[$key],SORT_DESC);
        //       //print "<pre>";print_r($all_product_totals[$key]); print "</pre>";
        //       current($all_product_totals[$key]);
        //       $return_data[$monthName][$key]['highest'] = key($all_product_totals[$key]); 
        //       end($all_product_totals[$key]);
        //       $return_data[$monthName][$key]['least'] = key($all_product_totals[$key]); 
        //       /******* Finding Top 5 Items Sold ************/
        //    }
            /********** Calculating Totals **********/
          }
        /**** Highest & Lowest product ****/
      }
      /**** Retrieving highest & lowest product ****/
      
      $month_counter++;
    }
    //print "<pre>"; print_r($return_data);print "</pre>";  
    return $return_data;
  }

  public function shopProductStatistics()
  {
  	 	foreach (STORES as $storename => $storedb)
  	 	{
  	 		 /**** Retrieving total revenue ******/
          $dbres = $this->load->database($storedb,TRUE);
          $tablename = "full_product_details";
          $retrievedData = $dbres->get($tablename);

          if(!empty($retrievedData))
          {
          	 $productNetWorth = $productDetails = array();
          	 foreach ($retrievedData->result() as $productData)
          	 {
          	 	 $totalAmount = $productData->Current_Qty * $productData->Unit_Price;
          	 	 if(array_key_exists($productData->Description, $productDetails))
						  $productDetails[$productData->Description] +=  $totalAmount;
          	 	 else
						  $productDetails[$productData->Description] = $totalAmount;

          	 	 $productNetWorth[] = $totalAmount;
				 }

				 $returnData[$storename] = array(
						'data' => $productDetails,
						'netWorth' => number_format(array_sum($productNetWorth), 2)
				  );
			 }
        /**** Retrieving total revenue ******/
      }
    	//print "<pre>"; print_r($return_data);print "</pre>";
    	return $returnData;
  }

}//End of class
