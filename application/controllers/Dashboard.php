<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	
	/***********************************************
		Constructor + Initialization
	************************************************/
	public function index() 
	{
		if (isset($_SESSION['username'])) 
		{
			$this->load->model('Universal_Retrieval');

      if(in_array('STATISTICS',$_SESSION['rows_exploded']) )
        redirect('dashboard/statistics');

      elseif(in_array('POS',$_SESSION['rows_exploded']) )
        redirect('dashboard/pos');

      elseif(in_array('REPORT',$_SESSION['rows_exploded']) )
        redirect('administration/report');

      elseif(in_array('SUPCUST',$_SESSION['rows_exploded']) )
        redirect('backOffice/vendor');

      elseif(in_array('PROD',$_SESSION['rows_exploded']) )
        redirect('backoffice/manage_product');

      elseif(in_array('CASH',$_SESSION['rows_exploded']) )
        redirect('backoffice/cash_manage');

      elseif(in_array('USERS',$_SESSION['rows_exploded']) )
        redirect('administration/users');        

      elseif(in_array('ROLES',$_SESSION['rows_exploded']) )
        redirect('administration/priviledges');

      elseif(in_array('SETTINGS',$_SESSION['rows_exploded']) )
        redirect('report/report');        
    }
		else 
			redirect('access');
	}
		/***********************************************
			Statistics Dashboard
		************************************************/
		public function master() 
		{
			if(in_array('STATISTICS', $_SESSION['rows_exploded'])) :
				# Loading Model 
				$this->load->model('Universal_Retrieval');

				$all_prod = $this->Universal_Retrieval->All_Info('full_product_details');	

				/************* Variable Declarations *************/
				$data = [
					'ofs' => 0,
					'availstock' => 0,
					'totalSales' => 0,
					'totalCustomers' => 0
				];
				/************* Variable Declarations *************/

				/*************** Out Of Stock Product Retrieve ****************/
	     	if(!empty($all_prod)) : 
		      foreach ($all_prod As $prod_det) :
		      	 # Definning All Criticals
		      	 if( $prod_det->Current_Qty <= 0 )
		      	  $data['ofs']  += 1;
		      endforeach;
	      endif;
	      /*************** Out Of Stock Product Retrieve ****************/

	      /*************** Available Stock Product Retrieve *************/
	      if(!empty($all_prod)) : 
		      foreach ($all_prod As $prod_det) :
		      	 # Definning All Criticals
		      	 if( $prod_det->Current_Qty != 0 )
		      	  $data['availstock']  += 1;
		      endforeach;
	      endif;
	      /*************** Available Stock Product Retrieve *************/

	      /*************** Daily Total Sales Retrieve *************/
	      $whereCondition = [ 'employee_id' => $_SESSION['employee_id'] ];

				$likecondition = [ 'date_withdrawn' => gmdate('Y-m-d') ];
				
				$dailyreportsum = $this->Universal_Retrieval->ret_data_with_s_cond_where_like('product_transact',$whereCondition,$likecondition);

				if(!empty($dailyreportsum))
				{
					foreach ($dailyreportsum as $value) 
					{
						$data['totalSales'] += $value->tot_cost;
					}
				}
				/*************** Daily Total Sales Retrieve *************/

				/*************** Total Customers Retrieve *************/
	      $allcustomers = $this->Universal_Retrieval->All_Info('product_customers');

				if(!empty($allcustomers))
					$data['totalCustomers'] = count($allcustomers);

				/*************** Total Customers Retrieve *************/
				$headertag['title'] = "Dashboard";
				$headertag['totalSales'] = $data['totalSales'];
	      $this->load->view('headtag1',$headertag);
	      $this->load->view('master',$data);
	      $this->load->view('footer');
	      /*************** Total Customers Retrieve *************/

	    else :
	      redirect('dashboard');

	     endif;
		}
	/******************************	Interfaces ****************************************/
		/***********************************************
			Statistics Dashboard
		************************************************/
		public function statistics() 
		{
			if(in_array('STATISTICS', $_SESSION['rows_exploded'])) :
				# Loading Model 
				$this->load->model('Universal_Retrieval');
				$all_prod = $this->Universal_Retrieval->All_Info('full_product_details');
				/************* Variable Declarations *************/
					$data = [
						'ofs' => 0,
						'availstock' => 0,
						'totalSales' => 0,
						'totalCustomers' => 0
					];
				 $data['shopProductStatistics'] = $this->Universal_Retrieval->shopProductStatistics();
				 //print "<pre>"; print_r($data['shopProductStatistics']); print "</pre>"; exit;
				/************* Variable Declarations *************/

				/*************** Out Of Stock Product Retrieve ****************
		     	if(!empty($all_prod)) : 
			      foreach ($all_prod As $prod_det) :
			      	 # Definning All Criticals
			      	 if( $prod_det->Current_Qty <= 0 )
			      	  $data['ofs']  += 1;
			      endforeach;
		      	endif;
	      		/*************** Out Of Stock Product Retrieve ****************/

	      		/*************** Available Stock Product Retrieve *************
		      	if(!empty($all_prod)) : 
			      foreach ($all_prod As $prod_det) :
			      	 # Definning All Criticals
			      	 if( $prod_det->Current_Qty != 0 )
			      	  $data['availstock']  += 1;
			      endforeach;
		      	endif;
	      		/*************** Available Stock Product Retrieve *************/

	      		/*************** Daily Total Sales Retrieve *************/
		      	$whereCondition = [ 'employee_id' => $_SESSION['employee_id'] ];

					$likecondition = [ 'date_withdrawn' => gmdate('Y-m-d') ];
					
					$dailyreportsum = $this->Universal_Retrieval->ret_data_with_s_cond_where_like('product_transact',$whereCondition,$likecondition);

					if(!empty($dailyreportsum))
					{
						foreach ($dailyreportsum as $value) 
						{
							$data['totalSales'] += $value->tot_cost;
						}
					}
				/*************** Daily Total Sales Retrieve *************/

				/*************** Total Customers Retrieve *************/
	      		$allcustomers = $this->Universal_Retrieval->All_Info('product_customers');

				if(!empty($allcustomers))
					$data['totalCustomers'] = count($allcustomers);
				/*************** Total Customers Retrieve *************/
				$data['allstores_data'] = $this->Universal_Retrieval->all_stores_stat();
				//$data['monthly_stat'] = $this->Universal_Retrieval->monthly_reports_stat();
//				 print "<pre>"; print_r($data['allstores_data']); print "</pre>";
//				 exit;
				 $headertag['title'] = "Dashboard";
				 $headertag['totalSales'] = $data['totalSales'];
				 $this->load->view('headtag1',$headertag);
				 $this->load->view('dashboard',$data);
				 $this->load->view('footer');
	      		/*************** Total Customers Retrieve *************/

			else :
			redirect('Dashboard');

			endif;
		}

		public function getmonthlystat() 
		{
			if(in_array('STATISTICS', $_SESSION['rows_exploded'])) : 
				# Loading Model 
				$this->load->model('Universal_Retrieval');
				//print"<pre>"; print_r($this->Universal_Retrieval->monthly_reports_stat_new());
				#response
				print_r(json_encode($this->Universal_Retrieval->monthly_reports_stat_new()));
			else :
				print "";
			endif;
				
		}

		public function getmonthlystatold() 
		{
			if(in_array('STATISTICS', $_SESSION['rows_exploded'])) : 
				# Loading Model 
				$this->load->model('Universal_Retrieval');
				//print"<pre>"; print_r($this->Universal_Retrieval->monthly_reports_stat());
				#response
				//print_r(json_encode($this->Universal_Retrieval->monthly_reports_stat()));
			else :
				print "";
			endif;
				
		}

		/******************************
			Point OF Sale
		*******************************/
		public function pos() 
		{
			# loading register view
			if(isset($_SESSION['username'])) 
			{
				$this->load->model('Universal_Retrieval');

				# Retrieving Data
				$whereCondition = ['status != ' => 'deleted'];
        $data['desc_info']  = $this->Universal_Retrieval->ret_data_with_s_cond_where('product_desc',$whereCondition);
				$data['allproduct'] = $this->Universal_Retrieval->All_Info('full_product_details');
				$data['accounts'] = $this->Universal_Retrieval->All_Info('account_types');
				/********** Interface ***********/
					$title['title'] = "Point Of Sale";
					$this->load->view('headtag',$title);
					$this->load->view('sellingpoint',$data);
					$this->load->view('footer');
				/********** Interface ***********/
			} 
			else { redirect('access/login'); }
		}

		/*******************************
			Print View
	 	*******************************/
    public function posprint() 
    {
      if(isset($_SESSION['username']))
      {
        if(in_array('POS', $_SESSION['rows_exploded']) && isset($_POST) )
        {
          $this->form_validation->set_rules('prodname[]','Product Name(s)','required|trim');
          $this->form_validation->set_rules('qty[]','qunatities','required|trim');
          $this->form_validation->set_rules('unit[]','Units','required|trim');
          $this->form_validation->set_rules('tot_cost','Total Cost','required|trim');

		      if ($this->form_validation->run() === FALSE) 
		      {
		        $this->session->set_flashdata('error',validation_errors());
		        redirect('dashboard/pos');
		      }
		      else 
		      {  
		      	# Loading models...
          	$this->load->model('Universal_Retrieval');
          	$this->load->model('Universal_Update');

		      	# Variable Declaration
		      	$prodname = $this->input->post('prodname');
		      	$prodqty = $this->input->post('qty');
		      	$produnit = $this->input->post('unit');
		      	$prodtotcost = $this->input->post('tot_cost');

		      	# Looping Through the variables
		      	for($a = 0; $a < sizeof(@$prodname); $a++)
		      	{
		      		
		      		$temp = ['prod_name' => $prodname[$a]];
		      		$prod_id = $this->Universal_Retrieval->ret_data_with_s_cond_row('product_codes','prod_name',$temp); 
		      		@$savedata['prodids'] .= $prod_id->prod_id."|";
		      		
		      		/************ Reducing From Product Quantity ****************/
		      		$qty_ret = ['prod_id' => $prod_id->prod_id];
		      		
		      		$ret_res= $this->Universal_Retrieval->ret_data_with_s_cond_row('product_details','prod_id',$qty_ret); 
		      		
		      		$prod_qty = $ret_res->avail_qty;

		      		if($prod_qty < $prodqty[$a])
		      		{
		      			$this->session->set_flashdata('error',"{$prodname[$a]} - Remaining Quanity : ".$prod_qty);
		      			redirect('Dashboard/POS');
		      		}

		      		else 
		      		{
								$data = [ 'avail_qty' => $prod_qty - $prodqty[$a] ];

			      		$where_condition = ['prod_id' => $prod_id->prod_id];

			      		$qty_red = $this->Universal_Update->Multiple_Update('product_details',$data,$where_condition); 
		      		}

		      		/************ Reducing From Product Quantity ****************/
		      		

		      		@$savedata['qtys'] .= $prodqty[$a]."|";
		      		@$savedata['units'] .= $produnit[$a]."|";
		      	}
		      	@$savedata['totcost'] = $prodtotcost;

		      	$printdata['printpreview'] = [
		      		'prodname' => $this->input->post('prodname'),
		      		'prodqty' => $this->input->post('qty'),
		      		'produnit' => $this->input->post('unit'),
		      		'prodtotcost' => $this->input->post('tot_cost')
		      	];

						$printdata['formsubmit'] = "
							<form action='".base_url()."Dashboard/Save_POS_INV' method='post' id='possave'>
								<input type='hidden' name='comb_prod_ids' value='".$savedata['prodids']."' />
								<input type='hidden' name='comb_qtys' value='".$savedata['qtys']." '/> 
								<input type='hidden' name='comb_units' value='".$savedata['units']."' /> 
								<input type='hidden' name='totalcost' value='".$savedata['totcost']."' /> 
							</form> ";

		      	 /********** Interface ***********************/    
          	  $headertag['title'] = "Product Vendors";
          	  $this->load->view('print',$printdata);
          	  $this->load->view('footer');
          	/********** Interface ***********************/
		      }
        } 
        else 
        {
          $this->session->set_flashdata('error',"Permission Denied. Contact Administrator");
          redirect('dashboard');
        }
      }
      else { redirect('access/login'); }
    }

	/***********************************************
			Setting View
	************************************************/
	public function salesreport() 
	{
		if(isset($_SESSION['username'])) 
		{
			if(in_array('POS',$_SESSION['rows_exploded']))
			{
				# Loading Model
				$this->load->model('Universal_Retrieval');

				$whereCondition = [ 'employee_id' => $_SESSION['employee_id'] ];

				$likecondition = [ 'date_withdrawn' => gmdate('Y-m-d') ];
				$like_condition = [ 'date_created' => gmdate('Y-m-d') ];
				
				$data['dailyreport'] = $this->Universal_Retrieval->ret_data_with_s_cond_where_like('product_transact',$whereCondition,$likecondition);
				$data['cashreport'] = $this->Universal_Retrieval->ret_data_with_s_cond_where_like('accounts_transactions',$whereCondition,$like_condition);

				if(!empty($data['dailyreport']))
				{
					foreach ($data['dailyreport'] as $value) 
					{
						@$data['totalSales'] += $value->tot_cost;
					}
				}

				/****** Interface ********************/
				$headertag['title'] = "Dashboard";
	      $this->load->view('headtag',$headertag);
	      $this->load->view('dreport',$data);
	      $this->load->view('footer');
	      /****** Interface ********************/
	    }
	    else { redirect('dashboard'); }
		} 
		else { redirect('access'); }
	}

	public function customersattach() 
	{
		if(isset($_SESSION['username'])) 
		{
			if(in_array('POS',$_SESSION['rows_exploded']))
			{
				# Loading Model
				$this->load->model('Universal_Retrieval');

				$whereCondition = [ 'employee_id' => $_SESSION['employee_id'] ];

				$likecondition = [ 'date_withdrawn' => gmdate('Y-m-d') ];
				$like_condition = [ 'date_created' => gmdate('Y-m-d') ];
				
				$data['dailyreport'] = $this->Universal_Retrieval->ret_data_with_s_cond_where_like('product_transact',$whereCondition,$likecondition);
				$data['cashreport'] = $this->Universal_Retrieval->ret_data_with_s_cond_where_like('accounts_transactions',$whereCondition,$like_condition);

				if(!empty($data['dailyreport']))
				{
					foreach ($data['dailyreport'] as $value) 
					{
						@$data['totalSales'] += $value->tot_cost;
					}
				}

				/****** Interface ********************/
				$headertag['title'] = "Dashboard";
	      $this->load->view('headtag',$headertag);
	      $this->load->view('customersattach',$data);
	      $this->load->view('footer');
	      /****** Interface ********************/
	    }
	    else { redirect('dashboard'); }
		} 
		else { redirect('access'); }
	}

	/***************************	Data Insertion	*****************************/
    
    /***********************************
			Save Expences
		***********************************/
    public function save_expenses() 
    {
    	if(isset($_SESSION['username']))
    	{
    		if(in_array('POS', $_SESSION['rows_exploded']) && isset($_POST['save_expenses']))
    		{
    			$this->form_validation->set_rules('acc_type','Account Type','required|trim');
		      $this->form_validation->set_rules('deb_amt','Debit Amount','trim');
		      $this->form_validation->set_rules('cred_amt','Credit Amount','trim');
		      $this->form_validation->set_rules('purpose','Purpose','required|trim');
		      $this->form_validation->set_rules('resulturl','URL','required|trim');

		      if ($this->form_validation->run() === FALSE) 
		      {
		        $this->session->set_flashdata('error',validation_errors());
						redirect($this->input->post('resulturl'));
		      }
		      else 
		      {  
		        # Loading Model
		        $this->load->model('Universal_Insertion');

		        if(empty($this->input->post('deb_amt')) && empty($this->input->post('cred_amt')))
		        {
		        	$this->session->set_flashdata('error',"Please Enter Debit OR Credit Amount");
							redirect($this->input->post('resulturl'));
		        }
		        
		        else
		        {
			        $data = [
								'acc_type_id' => base64_decode($this->input->post('acc_type')),
								'debit_amt' => (int)$this->input->post('deb_amt'),
								'credit_amt' => (int)$this->input->post('cred_amt'),
								'balance' => ((int)(int)$this->input->post('cred_amt') - (int)$this->input->post('deb_amt')),
								'purpose' => $this->input->post('purpose'),
								'employee_id' => $_SESSION['employee_id']
							];

			        $result = $this->Universal_Insertion->DataInsert('accounts_transactions',$data);

			        if($result)
								$this->session->set_flashdata('success',"Save Successful");

							else
								$this->session->set_flashdata('error',"Save Failed");

							redirect($this->input->post('resulturl'));
		        }
		      }
    		}
    		else
    		{
    			$this->session->set_flashdata('error','Permission Denied. Contact Administrator');
    		}
    	}
    	else { redirect('access/login'); }
    }
    

    /*********************************	Data Update	****************************/

    /******************************
	
    
	
	/**********************************************************************************************************************************************************
	********************************************************	Data Retrieval	*****************************************************************************
	**********************************************************************************************************************************************************/

	/***********************************************
				ATMs
	************************************************/

		/***********************************************
         AJAX Request to Fetch PRoducts
    ************************************************/
    public function prod_ret_ajax()
    {
      $this->form_validation->set_rules('productid','Product ID','required|trim');

      if ($this->form_validation->run() === FALSE) 
      {
        print "Product Retrieve Failed";
      }
      else 
      {  
        
        # Loading Model
        $this->load->model('Universal_Retrieval');

        $data = ['ProductCode' => base64_decode($this->input->post('productid'))];

        $result = $this->Universal_Retrieval->ret_data_with_s_cond('full_product_details','ProductCode',$data);

        if(!empty($result))
        {
          foreach ($result as $product) 
          {
            if($product->Promo_Price && $product->Promo_Qty)
            {
            	$promo_msg = "<span class='label label-warning'><span class='sign'>".@$product->Promo_Qty."&nbsp OR More, </span><b></b>GHȻ ".$product->Promo_Price." Each</span>";
            }
            print "<div class='col-lg-4 col-md-4 col-sm-6 col-xs-12' style='padding-bottom:10px;'>
            	<a class='card card-banner card-green-light' onclick='addtotally(this)' data-prodname='$product->Item_Name' data-prodid='".$product->Product_ID."' data-unitprice='".$product->Unit_Price."'  data-promounitprice='".$product->Promo_Price."' data-promounitqty='".$product->Promo_Qty."'>
            	<div class='card-body'>
            	<i class='icon fa fa-shopping-basket fa-2x'></i>
            	<div class='content'><div class='title'>".$product->Item_Name."</div>
            	".@$promo_msg."
            	<div class='value'><b><span class='sign'></span></b>GHȻ ".$product->Unit_Price."</div></div></div></a></div>";
          }
        }
      }
    }

		/***********************************************
         AJAX Request to Fetch PRoducts
    ************************************************/
    public function Save_POS_INV()
    { 
      $this->form_validation->set_rules('comb_prod_ids','Combined Product ID','required|trim');
      $this->form_validation->set_rules('comb_qtys','Combined Quantities','required|trim');
      $this->form_validation->set_rules('comb_units','Combined Unis','required|trim');
      $this->form_validation->set_rules('totalcost','Total Cost','required|trim');

      if ($this->form_validation->run() === FALSE) 
      {
        $this->session->set_flashdata('error',validation_errors());
				redirect('Dashboard/POS');
      }
      else 
      {  
        
        # Loading Model
        $this->load->model('Universal_Insertion');

        $data = [
					'prod_id' => $this->input->post('comb_prod_ids'),
					'description' => "customer",
					'qty_withdrawn' => $this->input->post('comb_qtys'),
					'unit_price' => $this->input->post('comb_units'),
					'tot_cost' => $this->input->post('totalcost'),
					'employee_id' => $_SESSION['employee_id']
				];

        $result = $this->Universal_Insertion->DataInsert('product_transact',$data);

        if($result)
					$this->session->set_flashdata('success',"Transaction Successful");

				else
					$this->session->set_flashdata('error',"Transaction Failed");

				redirect('dashboard/pos');
          
        }
    	}

    /***********************************************
         AJAX Request to Fetch PRoducts
    ************************************************/
    public function prod_ret_ajax_cat()
    {
      $this->form_validation->set_rules('desc_id','Category ID','required|trim');

      if ($this->form_validation->run() === FALSE) 
      {
        print "Product Retrieve Failed";
      }
      else 
      {  
        
        # Loading Model
        $this->load->model('Universal_Retrieval');

        $data = ['DescriptionID' => $this->input->post('desc_id')];

        $result = $this->Universal_Retrieval->ret_data_with_s_cond('full_product_details','DescriptionID',$data);

        if(!empty($result))
        {
          foreach ($result as $product) 
          {
            if($product->Promo_Price && $product->Promo_Qty)
            {
            	$promo_msg = "<span class='label label-warning'><span class='sign'>".@$product->Promo_Qty."&nbsp OR More, </span><b></b>GHȻ ".$product->Promo_Price." Each</span>";
            }
            print "<div class='col-lg-4 col-md-6 col-sm-6 col-xs-12' style='padding-bottom:10px;'>
            	<a class='card card-banner card-green-light' onclick='addtotally(this)' data-prodname='$product->Item_Name' data-prodid='".$product->Product_ID."' data-unitprice='".$product->Unit_Price."' data-promounitprice='".$product->Promo_Price."' data-promounitqty='".$product->Promo_Qty."'>
            	<div class='card-body'>
            	<i class='icon fa fa-shopping-basket fa-4x'></i>
            	<div class='content'><div class='title'>".$product->Item_Name."</div>
            	".@$promo_msg."
            	<div class='value'><b><span class='sign'></span></b>GHȻ ".$product->Unit_Price."</div></div></div></a></div>";
          }
        }
      }
    }

    /***********************************************
      AJAX Request to Return Row Data
    ************************************************/
    public function return_row_data()
    {
      $this->form_validation->set_rules('id','ID','required|trim');
      $this->form_validation->set_rules('tablename','tablename','required|trim');

      if ($this->form_validation->run() === FALSE) 
      {
        print "/";
      }
      else 
      {  
        # Loading Model
        $this->load->model('Universal_Retrieval');

        $data = [ 'id' => base64_decode($this->input->post('id')) ];
        $tablename = $this->input->post('tablename');

        $result = $this->Universal_Retrieval->ret_data_with_s_cond_row($tablename,'id',$data);

        if($result)
          print $result->acc_desc."/".$result->acc_ref_no;
      }
    }

   /***********************************************
    AJAX Request to Return Row Data
   ************************************************/
   public function return_daily_salesreport() {
      $this->form_validation->set_rules('trans_id','Transaction ID','required|trim');
      $this->form_validation->set_rules('resource','Resource','required|trim');

      if ($this->form_validation->run() === FALSE)
         print false;
      else {
         # Loading Model
         $this->load->model('Universal_Retrieval');
         $data = ['id' => $this->input->post('trans_id')];
         $result = $this->Universal_Retrieval->ret_data_with_s_cond('product_transact','id',$data, $resource = $this->input->post('resource'));

        if($result)
        {
        	$counter = 1;

        	foreach($result As $res) 
        	{
	        	$prods 	= explode('|',$res->prod_id);
	        	$qty 		= explode('|',$res->qty_withdrawn);
	        	$unit 	= explode('|',$res->unit_price);

	        	$prods_size = sizeof($prods);

	        	for($a = 0; $a < $prods_size; $a++) 
	        	{
	        		if(!empty($prods[$a])) :

		        		/***** Retrieving Product Name ********/
			        		$data = ['prod_id' =>  $prods[$a]];

			        		$product = $this->Universal_Retrieval->ret_data_with_s_cond_row('product_codes','prod_id',$data);
			              
			            if(!empty($product->prod_name))
			              $productname = $product->prod_name;

			            else
			            	$productname = "";
			          /***** Retrieving Product Name ********/
			          $qty_single = $qty[$a];

			          $unit_single = $unit[$a];

			          $display = "<tr><td>$counter</td><td>$productname</td><td>$res->description</td><td>$qty_single</td><td>$unit_single</td><td>".$qty_single * $unit_single."</td></tr>";
		          	
		          	$counter++;

		          	print $display;
		          endif;
		        }
	        }
        }
        
        else
        print false; exit();
      }
    }

    

}//End of Class
